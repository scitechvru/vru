-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 28, 2021 at 09:06 AM
-- Server version: 5.7.15-log
-- PHP Version: 7.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ds_scitech`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('superadmin','admin') NOT NULL,
  `major_admin` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `username`, `password`, `status`, `major_admin`, `last_login`, `updated_at`, `created_at`) VALUES
(1, 'เขมศักดิ์', 'เพ็ชรดีคาย', 'Khemmasak.phet', '$2y$10$4Lt7rTiLjxPQiMZJnijGKuoJarCeBi9Bp0zbTcFPRFb1rD5T9qFry', 'superadmin', 'OF', '2021-12-28 14:38:24', '2021-12-16 16:35:03', '2021-12-16 16:35:03'),
(2, 'เขมศักดิ์', 'เพ็ชรดีคาย', 'khemmasak.p', '$2y$10$0Kxjk3FeOZSmHk9TH2.DBOmupe.2s9RID9xCa3heo.zvfAEUFZtW.', 'admin', 'OF', '2021-12-28 14:21:36', '2021-12-22 09:51:46', '2021-12-22 09:51:46'),
(3, 'เบญญา', 'นามณรงค์', 'benya.n', '$2y$10$1yW5w7oLUS/0QwocakB4POKV2OCRXSFLcXw.RV1Dl8bPD7Ubuow.u', 'admin', 'OF', '2021-12-28 11:07:00', '2021-12-28 11:07:00', '2021-12-28 11:07:00'),
(4, 'เขมศักดิ์', 'เพ็ชรดีคาย', 'admin', '$2y$10$1SRHpqXz26SAzHOvGnDzouYaTYoSrwGbOiaIgCWbvWtRXCcsHAlHa', 'admin', 'IT', '2021-12-28 14:18:06', '2021-12-28 14:17:55', '2021-12-28 14:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `adminlogs`
--

CREATE TABLE `adminlogs` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `admin` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adminlogs`
--

INSERT INTO `adminlogs` (`id`, `ip`, `device`, `os`, `browser`, `page`, `admin`, `created_at`) VALUES
(1, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์-เพ็ชรดีคาย', '2021-12-28 12:41:37'),
(2, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:42:15'),
(3, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:42:17'),
(4, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:42:29'),
(5, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:43:26'),
(6, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:43:30'),
(7, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:43:35'),
(8, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:43:36'),
(9, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:43:38'),
(10, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:46:43'),
(11, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:47:13'),
(12, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:44'),
(13, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:45'),
(14, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:47'),
(15, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:49'),
(16, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:50'),
(17, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:51'),
(18, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:48:56'),
(19, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:52:18'),
(20, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 12:53:49'),
(21, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:03:43'),
(22, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:07:00'),
(23, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:07:48'),
(24, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:07:59'),
(25, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:08:17'),
(26, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:08:20'),
(27, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:09:06'),
(28, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:09:46'),
(29, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:11:33'),
(30, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:09'),
(31, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:11'),
(32, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:12'),
(33, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:12'),
(34, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:12'),
(35, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:35'),
(36, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:12:49'),
(37, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:15:22'),
(38, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:15:36'),
(39, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:17:10'),
(40, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:19:09'),
(41, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:19:36'),
(42, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:20:48'),
(43, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:21:37'),
(44, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:21:43'),
(45, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:23:09'),
(46, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:23:12'),
(47, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:25:36'),
(48, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:26:56'),
(49, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:27:21'),
(50, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:30:26'),
(51, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:31:06'),
(52, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:31:07'),
(53, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:35:48'),
(54, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:36:14'),
(55, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:36:26'),
(56, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:37:21'),
(57, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:37:36'),
(58, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:38:49'),
(59, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:44:48'),
(60, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:45:41'),
(61, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-edit', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:47:33'),
(62, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:48:26'),
(63, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:49:32'),
(64, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:49:35'),
(65, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 13:49:39'),
(66, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:01:07'),
(67, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:01:09'),
(68, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:06:45'),
(69, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:17:31'),
(70, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:17:35'),
(71, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:17:56'),
(72, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:18:06'),
(73, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:19:54'),
(74, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:21:36'),
(75, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:22:02'),
(76, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:23:35'),
(77, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:23:36'),
(78, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:32:43'),
(79, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:39:04'),
(80, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:39:07'),
(81, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:40:01'),
(82, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:52:51'),
(83, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 14:55:36'),
(84, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:01:31'),
(85, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:01:36'),
(86, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:02:08'),
(87, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:02:38'),
(88, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:07:28'),
(89, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:08:18'),
(90, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:09:16'),
(91, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:11:14'),
(92, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:11:33'),
(93, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:25:44'),
(94, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:26:13'),
(95, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:26:49'),
(96, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:27:38'),
(97, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:32:05'),
(98, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:32:53'),
(99, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:33:39'),
(100, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:34:22'),
(101, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:35:21'),
(102, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:35:58'),
(103, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:36:58'),
(104, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:37:49'),
(105, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:40:06'),
(106, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:43:03'),
(107, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:43:25'),
(108, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:43:43'),
(109, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:47:32'),
(110, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:51:17'),
(111, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:51:20'),
(112, '::1', 'Computer', 'Windows 10', 'Chrome', 'form-create', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:53:28'),
(113, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', 'เขมศักดิ์ เพ็ชรดีคาย', '2021-12-28 15:59:40');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `status` enum('true','false') NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `status` enum('true','false') NOT NULL DEFAULT 'false',
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE `tb_banner` (
  `id_ban` int(11) NOT NULL COMMENT 'รหัสbanner',
  `name_ban` text NOT NULL COMMENT 'ชื่อbanner',
  `major_ban` text NOT NULL,
  `title_ban` text NOT NULL COMMENT 'หัวข้อbanner',
  `detail_ban` text NOT NULL COMMENT 'รายละเอียดbanner',
  `link_ban` text NOT NULL,
  `status_ban` enum('true','false') NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_files`
--

CREATE TABLE `tb_files` (
  `id_files` int(11) NOT NULL,
  `name_files` text NOT NULL,
  `link_files` varchar(255) NOT NULL,
  `title_files` varchar(255) NOT NULL,
  `type_files` varchar(255) NOT NULL,
  `status_files` enum('true','false') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gf`
--

CREATE TABLE `tb_gf` (
  `id_gf` int(11) NOT NULL,
  `title_gf` varchar(255) NOT NULL,
  `link_gf` varchar(255) NOT NULL,
  `file_gf` varchar(255) NOT NULL,
  `type_gf` tinyint(4) NOT NULL,
  `status_gf` enum('true','false') NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='GreenOffice';

-- --------------------------------------------------------

--
-- Table structure for table `tb_major`
--

CREATE TABLE `tb_major` (
  `id_major` int(11) NOT NULL,
  `nameth_major` text NOT NULL,
  `nameen_major` text NOT NULL,
  `int_major` text NOT NULL,
  `link_major` text NOT NULL,
  `status_major` enum('true','false') NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_major`
--

INSERT INTO `tb_major` (`id_major`, `nameth_major`, `nameen_major`, `int_major`, `link_major`, `status_major`, `create_at`, `update_at`) VALUES
(1, 'สำนักงานคณบดี', 'Dean is Office', 'OF', 'http://sci.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(2, 'เทคโนโลยีสารสนเทศ', 'Information Technology', 'IT', 'http://comit.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(3, 'วิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม', 'Environmental Science and Technology', 'SET', 'http://set.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(4, 'เคมี (กำลังปรับปรุงหลักสูตร)', 'Chemistry', 'CHEM', 'http://chem.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(5, 'คหกรรมศาสตร์', 'Home Economics', 'HE', 'http://he.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(6, 'นวัตกรรมดิจิทัลและวิศวกรรมซอฟต์แวร์', 'Digital Innovation and  Software Engineering', 'DISE', 'http://dise.vru.ac.th/', 'true', '2021-12-20', '2021-12-20'),
(7, 'โภชนาการและการกำหนดอาหาร', 'Nutrition and Dietetics', 'NU', 'http://nu.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(8, 'การจัดการภัยพิบัติและบรรเทาสาธารณภัย', 'Disaster Management and Public Hazard Mitigation', 'DMPM', 'http://dmpm.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(9, 'คณิตศาสตร์ประยุกต์ (กำลังปรับปรุงหลักสูตร)', 'Applied Mathemathics', 'MATH', 'http://math.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(10, 'อาชีวอนามัยและความปลอดภัย', 'Occupational Health and Safety', 'OHS', 'http://ohs.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(11, 'วิทยาการคอมพิวเตอร์ ', 'Computer Science', 'CS', 'http://cs.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(12, 'นวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ', 'Food and Beverage  Innovation for Health', 'FB', 'http://fb.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(13, 'นวัตกรรมชีวผลิตภัณฑ์', 'Bachelor of Science Program in Bioproducts Innovation ', 'BIOT', 'http://biot.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(14, 'มาตรวิทยาอุตสาหกรรมและระบบคุณภาพ', 'Industrial Metrology and Quality System', 'IMQ', 'http://imq.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(15, 'ศูนย์วิทยาศาสตร์', 'Science Center', 'SCC', 'http://scc.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(16, 'ฟิสิกส์ประยุกต์ (กำลังปรับปรุงหลักสูตร)', 'Applied Physics', 'PHYS', 'http://phys.vru.ac.th', 'true', '2021-12-20', '2021-12-20'),
(17, 'คณะวิทยาศาสตร์และเทคโนโลยี (สำหรับ อ. ที่ไม่ได้เป็นผู้รับผิดชอบหลักสูตรของคณะ)', 'Sciecne and Technology', '9_SCI', '#', 'false', '2021-12-20', '2021-12-20'),
(18, 'วิทยาศาสตร์และนวัตกรรมเพื่อการพัฒนา', 'Science and Innovation', 'SID', '#', 'true', '2021-12-20', '2021-12-20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_news`
--

CREATE TABLE `tb_news` (
  `id_news` int(11) NOT NULL,
  `type_news` text NOT NULL,
  `major_news` varchar(255) NOT NULL,
  `title_news` varchar(255) NOT NULL,
  `name_news` varchar(255) NOT NULL,
  `link_news` varchar(255) NOT NULL,
  `img_news` varchar(255) NOT NULL,
  `status_news` enum('true','false') NOT NULL,
  `date_news` date NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_per`
--

CREATE TABLE `tb_per` (
  `id_per` int(11) NOT NULL COMMENT 'รหัสพนักงาน',
  `user_per` varchar(255) NOT NULL COMMENT 'ชื่อผู้ใช้งาน',
  `pass_per` varchar(255) NOT NULL COMMENT 'รหัสผู้ใช้งาน',
  `idcard_per` varchar(20) NOT NULL COMMENT 'รหัสประจำประชาชน',
  `tname2_per` text NOT NULL COMMENT 'อ. ผศ. รศ. ศ.',
  `tname1_per` text NOT NULL COMMENT 'ดร.',
  `tname_per` text NOT NULL COMMENT 'คำนำหน้าชื่อ',
  `fnamet_per` text NOT NULL COMMENT 'ชื่อไทยพนักงาน',
  `lnamet_per` text NOT NULL COMMENT 'นามสกุลพนักงาน',
  `fnamee_per` text NOT NULL COMMENT 'ชื่ออังกฤษพนักงาน',
  `lnamee_per` text NOT NULL COMMENT 'นามสกุลอังกฤษพนักงาน',
  `bdate_per` date NOT NULL COMMENT 'วันเกิด',
  `phone_per` text NOT NULL COMMENT 'เบอร์โทร',
  `email_per` text NOT NULL COMMENT 'อีเมล์',
  `img_per` text NOT NULL COMMENT 'รูปภาพพนักงาน',
  `dates_per` date NOT NULL COMMENT 'วันที่บันทึก',
  `type_per` int(2) NOT NULL COMMENT 'ประเภท',
  `major_per` text NOT NULL COMMENT 'สังกัด',
  `status_per` enum('true','false') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_per`
--

INSERT INTO `tb_per` (`id_per`, `user_per`, `pass_per`, `idcard_per`, `tname2_per`, `tname1_per`, `tname_per`, `fnamet_per`, `lnamet_per`, `fnamee_per`, `lnamee_per`, `bdate_per`, `phone_per`, `email_per`, `img_per`, `dates_per`, `type_per`, `major_per`, `status_per`) VALUES
(17, '580297529339ebc6064e981e3e0ff22f', 'd6ebf40f66ae39b2898e117571e510f3', '3150600360621', 'ผศ.', '', 'นาง', 'ไพรินทร์', 'มีศรี', 'Phairin', 'Meesri', '1978-09-22', '0840811725', 'phairin@vru.ac.th', '3150600360621.JPG', '2019-09-19', 1, 'IT', 'true'),
(18, 'cceebfa956393f11ba34a52a95534207', '833fe4eec14bff31eeb115323f946bfe', '3440300030186', '', '', 'นางสาว', 'พัสตราภรณ์ ', 'แสงปัญญา', '', '', '1982-04-21', '', '', '3440300030186.JPG', '2019-09-20', 2, 'OF', 'true'),
(24, '8b651f148cbcfbefcf146d5e48120998', 'cbcbc352080302b8fbdf0c83bb71bc25', '3140600022033', 'อ.', '', 'นาง', 'มัชฌกานต์  ', 'เผ่าสวัสดิ์', 'matchakan', 'phaosawad', '1973-03-09', '0898193666', 'matchakan@vru.ac.th', '3140600022033.JPG', '2019-09-24', 1, 'IT', 'true'),
(26, '91940d47245e11e4bf2ae491bfc240a3', 'c40354c0bb85ab7bc97c62a504bd53d7', '3260100611578', 'อ.', '', 'นางสาว', 'สิโรรัตน์', 'จั่นงาม', 'Sirorath', 'Channgam', '1976-11-16', '', 'sirorath@vru.ac.th', '3260100611578.JPG', '2019-09-24', 1, 'MATH', 'true'),
(27, '28c1f8f5030647673a1900e2fc8ef3f3', '1112fc98fb7c5a4ef208dd466dc4eb06', '1600100231597', 'อ.', '', 'นางสาว', 'จุฑารัตน์ ', 'โพธิ์หลวง', 'JUTARAT', 'PHOLUANG', '1987-04-05', '', 'jutarat@vru.ac.th', '1600100231597.JPG', '2019-09-24', 1, 'MATH', 'true'),
(28, 'c1cbf471de33a0b12ad16d64a5f4df54', '70d681bfe9dc04aad64b4cb9c9ed3353', '1100800183051', 'อ.', 'ดร.', 'นางสาว', 'ณัฐกาญจน์', 'นำพันธุ์วิวัฒน์', 'Nattakarn', 'Numpanviwat', '1985-09-27', '', 'nattakarn.num@vru.ac.th', '1100800183051.JPG', '2019-09-24', 1, 'MATH', 'true'),
(29, 'a1a4356a423bdb759c5dcbb9c53d7a9a', 'bf0b98c76708c833e0e8b0e225fe4851', '1103700751611', '', '', 'นาย', 'ศราวุฒิ', 'โปนา', 'sarawut', 'pona', '2535-04-01', '0881448908', 'sarawut.po@vru.ac.th', '1103700751611.JPG', '2019-09-24', 2, 'OF', 'true'),
(30, '447bc1b21e6bd371d7239a309f6998d4', 'b3de5b3187cee7d7e40d10869816e3f2', '3302000208721', 'อ.', '', 'นาย', 'ณัฐพงศ์ ', 'วัฒนศิริพงษ์', 'Nuttapong', 'Wattanasiripong', '1983-12-28', '0846596446', 'nuttapong@vru.ac.th', '3302000208721.JPG', '2019-09-24', 1, 'MATH', 'true'),
(31, '640350ea335dd88566656795f53f015b', 'a8eeaee1d90f5218e701bcaeeb427bb8', '3102101538303', 'ผศ.', 'ดร.', 'นางสาว', 'ดรุณี', 'หันวิสัย', 'darunee', 'hunwisai', '1977-02-27', '0830565113', 'darunee.hun@vru.ac.th', '3102101538303.JPG', '2019-09-24', 1, 'MATH', 'true'),
(35, 'e8bdb045b85dcfb1d52437325c2655ab', 'cc0a1abf9045228c6c1437eba98c0a1b', '3700800033548', 'ผศ.', '', 'นาย', 'วิวัฒน์', 'ชินนาทศิริกุล', 'WIWAT', 'CHINNATSIRIKUL', '1962-12-06', '0892228080', 'wiwat@vru.ac.th', '3700800033548.JPG', '2019-09-25', 3, 'CS', 'true'),
(36, '526361aafcf82d4152f4af882789c993', '57374ab2d1783494ec0b75893e98339e', '1539900058066', 'อ.', 'ดร.', 'นางสาว', 'ณภัทณ์จันทร์', 'ด่านสวัสดิ์', 'NAPATTCHAN', 'DANSAWAD', '1985-04-01', '', 'napattchan@vru.ac.th', '1539900058066.JPG', '2019-09-26', 1, 'IMQ', 'true'),
(37, 'f7a0be9a176075b601a29517c148dc2b', 'a2abc079a61dceed87bf881814a5a446', '5100599006251', 'ผศ.', 'ดร.', 'นาย', 'ปัณณ์รภัส', 'ถกลภักดี', 'Pannraphat', 'Takolpuckdee', '1975-03-19', '0950474444', 'pannraphat@vru.ac.th', '5100599006251.JPG', '2019-09-26', 3, 'SID', 'true'),
(39, '03c4ab8a3812a9f08242a39e74934c22', 'cfe6bc787198c62d6a4204752757cb0d', '1120600023599', 'อ.', '', 'นาย', 'วิศรุต', 'ขวัญคุ้ม', 'WISRUT', 'KWANKHOOM', '1985-07-09', '', 'wisrut@vru.ac.th', '1120600023599.JPG', '2019-10-02', 1, 'CS', 'true'),
(40, '2b82e7f666f298d2ffd487df0b35bb7a', 'ebcaed98632ef7594d67642fd08ff9c6', '3610600330281', '', '', 'นางสาว', 'ณัฐนิชา', 'กสิกรณ์', '', '', '2526-12-06', '', '', '3610600330281.JPG', '2019-10-03', 2, 'OF', 'true'),
(41, '351dc26beecdc999301b6654484866f0', '2069db7037f77a56ef2900ba6bb07344', '1149900051286', 'อ.', '', 'นางสาว', 'วิริยาภรณ์', 'กล่อมสังข์เจริญ', 'Wiriyabhorn', 'Klomsungcharoen', '1986-01-19', '', 'wiriyabhorn@vru.ac.th', '1149900051286.JPG', '2019-10-10', 1, 'MATH', 'true'),
(42, 'b8eb62801d08107756e44f58afaa27c1', 'df6518cd16d3a0144504601837c397ea', '1160100183384', 'อ.', '', 'นางสาว', 'ฐิติมา', 'เกษาหอม', 'Thitima', 'Kesahorm', '1987-05-06', '', 'thiti.kesa@gmail.com', '1160100183384.JPG', '2019-10-10', 1, 'MATH', 'true'),
(43, 'c2dc8c25db34935f20146479c5b6a22e', 'acd198ca2687de6ef1b6f90add98ca91', '1549900123305', '', 'ดร.', 'นางสาว', 'สุภาวิณี', 'ขันคำ', 'Supawinee', 'Khankham', '1987-08-18', '0813454440', 'supawinee@vru.ac.th', '1549900123305.JPG', '2019-10-15', 1, 'MATH', 'true'),
(44, '8dd53a9aa7d3f346d1b6653eac9ae5f0', '59b92b0b71a7f6f0ed8a447c2bd438ae', '3110300254826', 'ผศ.', 'ดร.', 'นางสาว', 'นิสา', 'พักตร์วิไล', 'Nisa', 'Pakvilai', '1979-03-27', '0818398196', 'nisa@vru.ac.th', '3110300254826.JPG', '2019-10-18', 1, 'SET', 'true'),
(45, '47bbc6727664f14515e41f2a4507ef6f', '029e508b4efdde00682f3aafc8f59be0', '3150600617177', 'ผศ.', 'ดร.', 'นางสาว', 'ณพัฐอร', 'บัวฉุน', 'Napattaorn', 'Buachoon', '1979-11-30', '', '', '3150600617177.JPG', '2019-10-18', 1, 'CHEM', 'true'),
(46, '4fe10dc14bd39f6a82110dec09630d80', '95c0c8da99ffdef81e29a6fc8c22f785', '3720700556301', 'ผศ.', 'ดร.', 'นาง', 'ณฐกมลวรรณ', 'ศรีจั่นเพชร', '', '', '1975-09-30', '', '', '3720700556301.JPG', '2019-10-18', 3, 'CHEM', 'true'),
(47, '4b29bafbb2a1e001f3810e15278a3c52', '0cc616871dc0a3a0cd1eeb239bb727d4', '3130100357885', 'รศ.', 'ดร.', 'นาย', 'มานะ', 'ขาวเมฆ', 'Mana', 'Kaomek', '1963-07-10', '0922619662', 'mana@vru.ac.th', '3130100357885.JPG', '2019-10-18', 3, 'CHEM', 'true'),
(48, '67101dac2e26cfce434e6a8ea026e8c4', 'e5e540c3af7b7e4ae8a0b325acf800c4', '3100500700480', 'อ.', 'ดร.', 'นาย', 'ปรินทร', 'เต็มญารศิลป์', '', '', '1980-08-15', '', '', '3100500700480.JPG', '2019-10-19', 1, 'CHEM', 'true'),
(49, '1a3415ce4d4e577cfc5681bf6dab22fb', 'f569ce3d227a597bed3d50299fcd812b', '1959900318838', '', '', 'นางสาว', 'สุชัญญา', 'ปิ่นสวัสดิ์', '', '', '1992-06-19', '', '', '1959900318838.JPG', '2019-10-21', 2, 'HE', 'true'),
(50, '89a7244693720924b4a09ed31ca91a4e', 'af4b38e3c3b7a39c2bfe817046871509', '1309900171662', 'อ.', 'ดร.', 'นางสาว', 'สุวิมล', 'สืบค้า', '', '', '1986-02-02', '', '', '1309900171662.JPG', '2019-10-21', 1, 'CHEM', 'true'),
(51, '03ad596b142d70c49fa736c0b186618d', '80d8449226ceeec9f846a482c4491346', '3349900674911', 'ผศ.', 'ดร.', 'นางสาว', 'พรรณวิภา ', 'แพงศรี', '', '', '1977-12-01', '', 'phanwipa@vru.ac.th', '3349900674911.JPG', '2019-12-01', 1, 'BIOT', 'true'),
(52, '70c702a25ba060f5e985fb5026b86809', '003ac6d52bb5fa181546e5210c849f53', '1679900372771', '', '', 'นาย', 'เขมศักดิ์', 'เพ็ชรดีคาย', 'Khemmasak', 'Phetdeekhai', '1996-08-09', '0982533106', 'khemmasak.phet@vru.ac.th', '1679900372771.JPG', '2019-12-09', 2, 'OF', 'true'),
(56, '4b8e95f4a78e46a7ea4a174eed8e9c17', '64ea56c708794e8dbf5a56158acf78cf', '3451100630199', 'ผศ.', 'ดร.', 'นางสาว', 'ณัฐสิมา', 'โทขันธ์', '', '', '1978-01-01', '', '', '3451100630199.JPG', '2019-12-23', 1, 'SET', 'true'),
(57, 'a914e5a05b8882299595a54ee6bc9f0e', '8df2e8bd2510115bf193f95bfc2dc83e', '1720200064951', 'อ.', 'ดร.', 'นางสาว', 'บุษยา', 'จูงาม', '', '', '1987-05-19', '', '', '1720200064951.JPG', '2019-12-23', 1, 'OHS', 'true'),
(58, 'a652083f5fc9f8cf14fcb6aa9f822768', '9481a9e9d30f4d28caa4fdeef026d460', '1719900256176', 'อ.', '', 'นางสาว', 'พชรกมล', 'กลั่นบุศย์', 'Patcharakamon', 'Klunbut', '1991-10-14', '0918199224', 'patcharakamon@vru.ac.th', '1719900256176.JPG', '2019-12-25', 1, 'OHS', 'true'),
(59, 'f50b202b3c93b88d4597018df4e9d8e2', 'e10adc3949ba59abbe56e057f20f883e', '1369900002828', 'อ.', 'ดร.', 'นาง', 'จินต์จุฑา', 'ขำทอง', '', '', '0000-00-00', '', '', '1369900002828.JPG', '2019-12-25', 1, 'OHS', 'true'),
(60, '8b3bbff7f4a3a598d328df0966463a84', '00fce1b49b2a7cbd1e00792251bc85a8', '3920600450763', 'อ.', '', 'นาง', 'อรวรรณ', 'ชำนาญพุดซา', 'Orawan', 'Chamnanphudsa', '1976-06-03', '0886346888', 'orawan.cham@vru.ac.th', '3920600450763.JPG', '2019-12-25', 1, 'OHS', 'true'),
(61, '2de9ba43040041441c734bc0123b195f', 'a540a3363090ba70d2dae7d889d62e8a', '3250400106950', 'ผศ.', '', 'นางสาว', 'มณทิพย์', 'จันทร์แก้ว', 'Montip', 'Jankaew', '1977-02-07', '0945511561', 'montip@vru.ac.th', '3250400106950.JPG', '2020-02-27', 1, 'SET', 'true'),
(62, 'f3ab39e9e568e5ca6a5b698acb643fdd', '489096c87d08f1252f537ba41b8eeeac', '1710500008771', 'ผศ.', 'ดร.', 'นาย', 'วีระวัฒน์', 'อุ่นเสน่หา', '', '', '1984-06-01', '', '', '1710500008771.JPG', '2020-03-04', 1, 'SET', 'true'),
(65, 'd7a79ac97bd0d293d29e33c816da7a59', '7ec7acb88a2d0583113da625d5e0f78f', '3100202592824', 'อ.', 'ดร.', 'นาย', 'นพรัตน์', 'ไวโรจนะ', 'Nopparat ', 'Wairojjana ', '1976-03-16', '0982702188', 'Nopparat@vru.ac.th', '3100202592824.JPG', '2020-03-22', 1, 'MATH', 'true'),
(66, 'c734f849874965dcbeb2c9505d76ee14', '66ac8397b903f2dbf3048fc2e91817cf', '1539900310938', 'อ.', '', 'นางสาว', 'ศิรภัสสร', 'พันธะสา', '', '', '1991-04-06', '', '', '1539900310938.JPG', '2020-04-02', 1, 'DMPM', 'true'),
(67, '599eecaa3651b8e9e06cbabc13e86c34', '9a981090c1029e6cf978e795e1a396d6', '3579900092546', 'ผศ.', '', 'นางสาว', 'ณัฏฐิรา', 'ศุขไพบูลย์', '', '', '1973-12-25', '', '', '3579900092546.JPG', '2020-04-14', 3, 'CS', 'true'),
(68, '3484dc73d49d8f22d3c70304c15d166a', 'adad26db89c004ac42f231c6a64aaded', '3200200035358', 'รศ.', '', 'นาย', 'คชินทร์ ', 'โกกนุทาภรณ์', 'kachin', 'kokanutapon', '1976-01-10', '0972145096', 'kachin@vru.ac.th', '3200200035358.JPG', '2020-04-15', 1, 'MATH', 'true'),
(69, 'dd91743cffebc0c651116625beb19a72', 'f1dea1d8e73c5d90ff76ebb1f60b4478', '1101401142823', 'อ.', '', 'นางสาว', 'สุนันทา', 'ศรีโสภา', 'Sunanta', 'Srisopha', '1987-10-23', '', 'sunanta.sri@vru.ac.th', '1101401142823.JPG', '2020-04-16', 1, 'MATH', 'true'),
(70, '0f638864e4305016a7636b44e0dfaf26', '3a73cce4b259dc86edb9bd49a2080681', '1710900115469', 'อ.', 'ดร.', 'นางสาว', 'นพมาศ', 'ประทุมสูตร', 'Noppamas', 'Pratummasoot', '1989-06-06', '0890867739', 'noppamas.pra@vru.ac.th', '1710900115469.JPG', '2020-04-16', 1, 'PHYS', 'true'),
(71, '676820231ca0837c97e1bb8d1203e242', '1e0d010050e7c140086a5a206ac10799', '3409900842198', 'ผศ.', '', 'นาย', ' วรายุทธ์', 'อัครพัฒนพงษ์', '', '', '1964-03-12', '', '', 'user-solid.svg', '2020-04-16', 3, 'IMQ', 'true'),
(72, '2870e253e70176443c5b6555c40616f2', '14347eb04dc6ead075c55f2a4a45cff6', '1929900158032', 'อ.', '', 'นางสาว', 'ศกุนตาล์ ', 'มานะกล้า', 'Sakunta ', 'Manakla', '1988-08-11', '', 'sakunta@vru.ac.th', '1929900158032.JPG', '2020-04-16', 1, 'NU', 'true'),
(73, '2eb8989a16f1598df8fe732ae3c9488b', '669a5ad782492ff573ad84ccdafca1cf', '3170500118845', '', '', 'นางสาว', 'ทวีพร', 'พันสวัสดิ์', '', '', '1982-03-20', '', '', '3170500118845.JPG', '2020-04-16', 2, 'NU', 'true'),
(74, '2875ab3bfff86d0995c2f2fbf44db089', '10d11fdd7ef4a05032c116b077b298b6', '1929900130391', 'อ.', '', 'นางสาว', 'ใยแพร', 'ชาตรี', '', '', '1987-10-08', '', '', '1929900130391.JPG', '2020-04-16', 1, 'NU', 'true'),
(75, 'a0a1edd5378cf0570c07d96a1d480836', 'f53d934cf4b25bdac485b0d5154bed97', '2809900030477', 'อ.', '', 'นางสาว', 'จุฑาวรรณ ', 'นวลจันทร์คง', '', '', '1991-05-12', '', '', '2809900030477.JPG', '2020-04-16', 1, 'NU', 'true'),
(76, 'ae81bddd9cfa393c22a76982fdd903a7', 'b1b357c1cbe6cdb8d5c2f80e65ca1894', '1449900136939', 'อ.', '', 'นางสาว', 'ปัทมาภรณ์ ', 'เจริญนนท์', 'Pattamaporn', 'Jaroennon', '1988-09-05', '0872236315', 'pattamaporn@vru.ac.th', '1449900136939.JPG', '2020-04-16', 1, 'NU', 'true'),
(77, '5953da62ae0a943b8d849244f11a63c7', '265607a85ba3a6c9244bc508f3bbefae', '1139900075746', '', '', 'นางสาว', 'ภัศรา  ', 'วงค์สุดี', '', '', '1989-06-29', '', '', '1139900075746.JPG', '2020-04-16', 2, 'SET', 'true'),
(78, 'aae7509655381d48c8147fb64678634d', '5814b909033767625ebf232934247beb', '3100201205835', 'ผศ.', '', 'นางสาว', 'เบญจมาศ', 'แก้วนุช', 'Benjamas', 'Keawnuch', '1952-08-05', '0898375899', 'banben9954@gmail.com', '', '2020-04-16', 1, 'PHYS', 'true'),
(79, 'a6cc1c9ae9a94d2cb210264a10b49930', '720f5e7832df9a786c3ce4e38075f7b5', '1720900148405', 'อ.', '', 'นางสาว', 'สุจา​ริณี​', 'สังข์​วรรณะ​', '', '', '1991-03-22', '', '', '1720900148405.JPG', '2020-04-16', 1, 'NU', 'true'),
(80, 'eaf761869bfe93e4cd6d418b86e335f7', '365dd8377a0b4f842b8d9f21e176809d', '3450900261236', 'ผศ.', '', 'นาย', 'โยธิน', 'กัลยาเลิศ', '', '', '1977-03-02', '', '', '3450900261236.JPG', '2020-04-17', 1, 'PHYS', 'true'),
(99, 'b94e9ed50c105cff95dc4a856bda60a7', 'b7540bbd6d0eb8bd542e592c5cd2b4d2', '3410100150854', '', '', 'นาง', 'กนกพร', 'สัณห์ฤทัย', 'kanokporn', 'sanrutai', '1969-12-22', '0894497419', 'pp3647@gmail.com', '3410100150854.JPG', '2019-09-24', 4, 'OF', 'true'),
(100, '5f5c3c997e38df56bd31378f142f5fa2', 'e619d87c034286a0902e9d442998a6cc', '1509900859123', 'อ.', '', 'นางสาว', 'สุรินทร์', 'อุ่นแสน', '', '', '1990-04-28', '', '', '1509900859123.JPG', '2020-04-26', 1, 'DISE', ''),
(101, '18bc61a42552c22ab56db0fef10827d9', '02041989', '1101500341018', 'อ.', '', 'นางสาว', 'ชลลดา', 'พละราช', 'Chonlada', 'Palarach', '1989-04-02', '0869465355', 'chonlada.pa@vru.ac.th', '', '2020-08-10', 1, 'OHS', ''),
(102, '9a558335566be639c2ffd15ed54e3eb9', 'd878855da231ada2b312f56ebf4338e4', '1309900760323', 'อ.', '', 'นางสาว', 'ขวัญแข', 'หนุนภักดี', 'KHWANKHAE', 'NUNBHAKDI', '1991-12-19', '0935153924', 'khwankhae@vru.ac.th', '1309900760323.JPG', '2020-08-20', 1, 'OHS', 'true'),
(103, '01ad977c26e8428fa2deb669bf359d66', '6493c6e7f53a1d0a14277ae6985328c2', '1179900123082', 'อ.', '', 'นางสาว', 'วัชราภรณ์', 'วงศ์สกุลกาญจน์', 'Watcharaporn', 'Wongsakoonkan', '1987-12-14', '0809047871', 'watcharaporn@vru.ac.th', '1179900123082.JPG', '2020-08-24', 1, 'OHS', 'true'),
(104, '1851292a79361c91718b153ace41de53', 'dc9574f65543018481710db8727609a9', '3850400100859', 'ผศ.', 'ดร.', 'นางสาว', 'เยาวภา', 'แสงพยับ', '', '', '1981-09-06', '', '', '3850400100859.JPG', '2020-08-24', 1, 'IMQ', 'true'),
(106, '514ccfe1f2bf4d80fcfbe57edeaacebf', '1f338a5cddd8e7500f46497f0f0e26fd', '3709700091731', 'ผศ.', '', 'นาย', 'วัฒนา ', 'อัจฉริยะโพธา', 'WATTANA', 'ASCHARIYAPHOTHA', '1976-10-31', '025290674', 'wattana@vru.ac.th', '3709700091731.JPG', '2020-09-17', 1, 'BIOT', 'true'),
(107, 'bdc44cbf3594109a4c73bdb7bbbe6a46', '51a4f55ef6b96ec33b0c0ae1490750b4', '3149900304156', 'ผศ.', 'ดร.', 'นางสาว', 'ปุณยนุช', 'นิลแสง', '', '', '1972-10-10', '', '', '3149900304156.JPG', '2020-09-17', 3, 'SID', 'true'),
(108, '541a73543dc372825f147fc1ccee7e29', 'f9c21ef6178bb8ad1738086445653f32', '3409900903880', 'ผศ.', '', 'นาย', 'ณฐพงศ์', 'เมธินธรังสรรค์', '', '', '1976-12-06', '', '', '3409900903880.JPG', '2020-09-17', 1, 'BIOT', 'true'),
(109, '6b51fafbac8a88b0110f796879ec05e0', '49f67f7e6be797b9ee3da486140bc901', '1459900006890', 'อ.', 'ดร.', 'นางสาว', 'มัทนภรณ์', 'ใหม่คามิ', '', '', '1984-06-18', '', '', '1459900006890.JPG', '2020-09-19', 1, 'BIOT', 'true'),
(110, '9d5d90f09e6087fd882ce9a660b59313', '5a8dbb229f9dbb927b26f7671be26574', '3100903239705', 'ผศ.', 'ดร.', 'นางสาว', 'ขนิษฐา', 'ภมรพล', '', '', '1979-04-06', '', '', '3100903239705.JPG', '2020-10-26', 3, 'SET', 'true'),
(111, '6b25a6861577451a24e40a0704424c10', 'c937fee15ff8c7f2487787599f964e29', '3120100701225', 'อ.', 'ดร.', 'นางสาว', 'ประภาพร', 'ชุลีลัง', '', '', '1964-10-15', '', '', '3120100701225.JPG', '2020-10-26', 3, 'SET', 'true'),
(112, '20ac3fcb0b2fad924073b14321318332', '6b974840939644eb87ee73531fee99f0', '3300600258132', 'อ.', '', 'นางสาว', 'ณหทัย', 'โชติกลาง', '', '', '1981-01-14', '', '', '3300600258132.JPG', '2020-10-26', 3, 'SET', 'true'),
(113, '1fb7490a74849a60154c11daa04a69d1', 'b182b8a4b08388f89287807bfb815375', '3729900217690', 'อ.', '', 'นางสาว', 'ณัฐกานต์', 'ทองพันธุ์พาน', '', '', '1978-05-15', '', '', '3729900217690.JPG', '2020-10-26', 3, 'SET', 'true'),
(114, '5ee8b89a1b49d07f052ba6ae6562c399', '22c4eddb1628a78069bdd53a02f365dd', '3170600234367', 'รศ.', 'ดร.', 'นางสาว', 'ศศมล', 'ผาสุข', '', '', '1958-12-15', '', '', '3170600234367.JPG', '2020-10-26', 3, 'SID', 'true'),
(115, '2d1977728d3a03e89d919062230ffe9e', '0595d0530c28794231462222a2b7c09c', '1449900010650', 'อ.', 'ดร.', 'นางสาว', 'อมตา', 'อุตมะ', '', '', '1984-08-17', '', '', '1449900010650.JPG', '2020-10-26', 3, 'OHS', 'true'),
(116, '3ed72be7b98e98eaa0c1f5936df90af5', '7a450d3d6070fe03728ef38e0f7e36b6', '3100502695491', 'อ.', 'ดร.', 'นางสาว', 'สัจนา', 'พัฒนาศักดิ์', '', '', '1977-07-29', '', '', '3100502695491.JPG', '2020-10-26', 3, 'NU', 'true'),
(117, '96777620fc3d9e97d6056608e1b63cd7', '0327744d502003414fc0cef5210646f7', '1409900093539', 'อ.', '', 'นางสาว', 'นันทิภา', ' แก้วลีลาวรรณ', '', '', '1984-11-23', '', '', '1409900093539.JPG', '2020-10-26', 3, 'NU', 'true'),
(118, '5ab89347499ce300c3781db975fd5964', 'e7fa6c161d2650f6520df40990dd0398', '3141600083157', 'ผศ.', '', 'นางสาว', 'อมีนา', 'ฉายสุวรรณ', '', '', '1997-10-04', '', '', '3141600083157.JPG', '2020-10-26', 3, 'IT', 'true'),
(119, 'da1af60537b1bfd72d54d0ea2445ce0a', '3c9399ebd09f24bb55b0086cb1242019', '3419900264291', 'อ.', '', 'นาย', 'ไชย', 'มีหนองหว้า', '', '', '1970-10-08', '', '', '3419900264291.JPG', '2020-10-26', 3, 'IT', 'true'),
(120, '6507d6784660a0c192cc74ed8620d5a7', 'df3b28587bcd90f91712a33b8787b110', '3129900357918', 'ผศ.', '', 'นางสาว', 'ทักษิณา', 'วิไลลักษณ์', '', '', '1971-05-21', '', '', '3129900357918.JPG', '2020-10-26', 3, 'IT', 'true'),
(121, '630c48aa9e4dd5d6ee23eca021e603dc', '71b21ffdac4549fa48e0b40488ea39b9', '3140100311478', 'ผศ.', '', 'นาง', 'กมลมาศ', 'วงษ์ใหญ่', 'kamolmas', 'wongyai', '1974-03-27', '0898888813', 'kamolmas@vru.ac.th', '3140100311478.JPG', '2020-10-26', 3, 'IT', 'true'),
(122, 'a0ba15ba64c7f9583985fdc022bf4f7d', 'b84ae627484d1124cccf08da432f655f', '3410400687837', 'ผศ.', '', 'นางสาว', 'อิงอร', 'วงษ์ศรีรักษา', '', '', '1972-08-10', '', '', '3410400687837.JPG', '2020-10-26', 3, 'IT', 'true'),
(123, '721ded51679bac4ef755c82604d28a9d', '1b99ce8d4b752adfec5accbb857c75ac', '3130200214318', 'ผศ.', '', 'นางสาว', 'อัจจิมา', 'มั่นทน', '', '', '1978-04-21', '', '', '3130200214318.JPG', '2020-10-26', 3, 'IT', 'true'),
(124, '01b76927a0fda0d6ecfeb29808e97515', '6bf9c5c5367eaf2bd4609207e7ee032f', '3170600520530', 'อ.', '', 'นาย', 'ชุมพล', 'จันทร์ฉลอง', '', '', '1974-08-27', '', '', '3170600520530.JPG', '2020-10-26', 3, 'IT', 'true'),
(125, 'e680a9f2c42ada7f937c7d0c0300d6c2', 'e5e540c3af7b7e4ae8a0b325acf800c4', '3101500329537', 'ผศ.', 'ดร.', 'นาย', 'กิตติศักดิ์', 'สิงห์สูงเนิน', 'kittisak', 'singsungnoen', '1980-08-15', '0613826151', 'kittisak.sing@vru.ac.th', '3101500329537.JPG', '2020-10-26', 3, 'IT', 'true'),
(126, '1dfda2b97dd3b20d0ac806542d674d1b', '811ebf1dbcda13229446ddf1f98b5179', '1200100174872', 'อ.', 'ดร.', 'นางสาว', 'สินีนาถ', 'สุขทนารักษ์', '', '', '1987-12-04', '', '', '1200100174872.JPG', '2020-10-26', 3, 'HE', 'true'),
(127, '098338e41b6cb16f2933796a43fccf5a', 'dac9eaa394f6468638c06b6d6c099ef8', '3100501079576', 'อ.', '', 'นางสาว', 'พัชรลักษณ์', 'วัฒนไชย', '', '', '1980-03-06', '', '', '3100501079576.JPG', '2020-10-26', 3, 'HE', 'true'),
(128, '08fa59231fb07f3e7d1e27bf7a8663de', '0cf29876413b1bf07bd1d3674db25d24', '3300100003381', 'อ.', '', 'นางสาว', 'จุรีมาศ', 'ดีอำมาตย์', '', '', '1980-12-24', '', '', '3300100003381.JPG', '2020-10-26', 3, 'HE', 'true'),
(129, 'f106d77a7a6ec75e7d76dc9ced8281f4', '2edf15d59667073b9350ac01e3a869b2', '3309901695025', 'ผศ.', '', 'นาง', 'เบญจางค์', 'อัจฉริยะโพธา', '', '', '1979-01-19', '', '', '3309901695025.JPG', '2020-10-26', 3, 'HE', 'true'),
(130, '6fbecd1a1dfc5da21cf4f4a6a5eb9f53', 'f5a331c1566784f68e16555b1ba8f2a5', '1450300045683', 'อ.', '', 'นางสาว', 'ลัดดาวัลย์ ', ' กงพลี', 'Laddawan', 'Kongplee', '1988-04-03', '0864295451', 'Laddawan.kong@vru.ac.th', '1450300045683.JPG', '2020-10-26', 3, 'FB', 'true'),
(131, '3c1a0058730d8b2af58c3c6989597a41', '65e2c5e95831e019e2a9a43187375d35', '3539900140061', 'ผศ.', '', 'นาง', 'กนกวรรณ', 'ปุณณะตระกูล', '', '', '1960-03-21', '', '', '3539900140061.JPG', '2020-10-26', 3, 'FB', 'true'),
(132, 'b2be1aefab37b5996524b110545fe989', '73f53a82509b31966cabacc55c0c00d0', '3130600001621', 'ผศ.', 'ดร.', 'นาย', 'รัตถชล', 'อ่างมณี', '', '', '1980-03-10', '', '', '3130600001621.JPG', '2020-10-26', 3, 'DMPM', 'true'),
(133, '9d2387e0ab1f92bfc83fd918debc4870', 'c37e9df9c8e115495c56f6bfefe4d770', '3770400048165', 'ผศ.', 'ดร.', 'นาย', 'ตีรณรรถ', 'ศรีสุนนท์', '', '', '1980-08-17', '', '', '3770400048165.JPG', '2020-10-26', 3, 'DMPM', 'true'),
(134, 'd8609bd9c02083e6ea881634969889a0', 'ad6e28386d47f37b4a7db9f2bf4dfb4c', '1459900206139', 'อ.', '', 'นาย', 'วีระศักดิ์', 'ศรีลารัตน์', '', '', '1990-08-22', '', '', '1459900206139.JPG', '2020-10-26', 3, 'FB', 'true'),
(135, '5872d05c24f6e0ed39b6091e6eb559fc', '6431b45e8a9520b68c902ebdb58365e6', '1820500071463', 'อ.', '', 'นาย', 'สมคิด', 'ตันเก็ง', '', '', '1990-10-12', '', '', '1820500071463.JPG', '2020-10-26', 3, 'DMPM', 'true'),
(136, '73c38414fe3fd600f55b22a80888bbde', 'ed9a69bd68a38bfa0bb7a041d0374c33', '3190600113853', 'อ.', '', 'นาย', 'นิธิพนธ์', 'น้อยเผ่า', '', '', '1978-04-13', '', '', '3190600113853.JPG', '2020-10-26', 3, 'DMPM', 'true'),
(137, '4620f3f8f5784bf25ef93ed88217ea07', '511557ded1d8431109881da1e419d3b1', '3130200168057', 'อ.', '', 'นาย', 'เศรษฐพงศ์', 'วงษ์อินทร์', 'Sethapong', 'Wong-In', '1975-08-07', '0855935525', 'sethapong@vru.ac.th', 'S__9019397.jpg', '2020-10-26', 3, 'DISE', 'true'),
(138, '85179604be11ad7c537bdfe877560384', 'bfdddae63a36dcb8da13b1090f77f1cb', '3309901150827', 'อ.', '', 'นางสาว', 'ปัณณรัตน์', 'วงศ์พัฒนานิภาส', '', '', '1998-02-25', '0655515142', 'pannarat@vru.ac.th', 'S__9019395.jpg', '2020-10-26', 3, 'DISE', 'true'),
(139, 'b5764f71d7229c56f422f9ae7eb39821', 'b9c7fa7d6a5ff1a4a1673449ca4e319a', '3550400231018', 'อ.', '', 'นางสาว', 'อรรถพร', 'ธนูเพ็ชร์', '', '', '1968-06-17', '', '', '3550400231018.JPG', '2020-10-26', 3, 'DISE', 'true'),
(140, '1c2810006f815d216b0ecb7f37301007', 'c36966a036cd2b2595c98bf3caaf203b', '3311300027098', 'อ.', '', 'นางสาว', 'ดาวรถา', 'วีระพันธ์', '', '', '1978-05-07', '', '', '3311300027098.JPG', '2020-10-26', 3, 'CS', 'true'),
(141, '0ac82a3d91534225a615419baab9a30a', '06beec52ee021d476f00d89c3bc1e4a9', '3329900151790', 'อ.', '', 'นางสาว', 'ประณมกร', 'อัมพรพรรดิ์', '', '', '1975-03-09', '', '', '3329900151790.JPG', '2020-10-26', 3, 'CS', 'true'),
(142, 'a2900582168555f358946b74ba9b80aa', 'd3b66cdb467eac668bb73632b11daaca', '3110400093981', 'ผศ.', '', 'นางสาว', 'สุนี', 'ปัญจะเทวคุปต์', '', '', '1961-11-15', '', '', '3110400093981.JPG', '2020-10-26', 3, 'CS', ''),
(143, 'd2367ff12ab7b03774d8a45af803bce3', 'fc726a247378748f2dc081e50bfbf0af', '1100800382274', 'อ.', '', 'นาย', 'ชวลิต', 'โควีระวงศ์', '', '', '1987-11-08', '', '', '1100800382274.JPG', '2020-10-26', 3, 'CS', 'true'),
(144, 'aa5a04867fb61833898dc21a03385b27', '915eab7af02634fa10d894f37a7142cb', '1101400783442', 'อ.', '', 'นางสาว', 'ณัฐรดี', 'อนุพงค์', '', '', '1986-09-14', '', '', '1101400783442.JPG', '2020-10-26', 3, 'CS', 'true'),
(145, '44c9a2600ea4f1151624eba2dc0d8af1', 'b9d57058cdaf05c03389be6119bac144', '1539900061067', 'อ.', 'ดร.', 'นางสาว', 'พชรวรรณ', 'รัตนทรงธรรม', '', '', '1985-04-29', '', '', '1539900061067.JPG', '2020-10-26', 3, 'CHEM', 'true'),
(146, '46974f80306ab9d99db9b2148a4e7fdb', '641e8a48e7a0250be7c0811fc8b2c2a0', '3160100811624', 'ผศ.', '', 'นางสาว', 'ดวงเดือน', 'วัฏฏานุรักษ์', '', '', '1972-07-22', '', '', '3160100811624.JPG', '2020-10-26', 3, 'BIOT', 'true'),
(147, '02e1e770e8bcd1a6456f29f4446044a7', 'da7eedb6e3c5834cd956070a0ebb1671', '3419900655649', 'รศ.', 'ดร.', 'นางสาว', 'นฤมล', 'ธนานันต์', '', '', '1970-06-08', '', '', '3419900655649.JPG', '2020-10-26', 3, 'BIOT', 'true'),
(148, '4a3e22ad35afd252350bcdfe34f4c988', 'e814c92e8e92d1f112fdc11b1d599673', '3169900213347', 'อ.', '', 'นางสาว', 'จิตติมา', 'กอหรั่งกูล', '', '', '1977-10-21', '', '', '3169900213347.JPG', '2020-10-26', 3, 'BIOT', 'true'),
(149, '392f9f7d2f5aad89d4276e08b93b481d', '88b4296eeb8f4ba15c46dba5c7598b94', '3440500097012', 'อ.', 'ดร.', 'นาย', 'วิชัย', 'กองศรี', '', '', '1973-01-09', '', '', '3440500097012.JPG', '2020-10-26', 3, 'PHYS', 'true'),
(150, 'a51967f72d62474a94cba6481dd252bf', '80ae33f9c7ca2adb2e695e385681bf01', '3260300110912', '', '', 'นาง', 'ตะวันฉาย', 'บุญประกอบศุภรางค์', '', '', '1973-04-28', '', '', '3260300110912.JPG', '2020-10-26', 2, 'SCC', 'true'),
(151, '9c81cb39710b7b8edfe0ceb90a584a55', '9d2cb3df7889f2bb6077722b92d7092c', '3130700369515', '', '', 'นาย', 'เนติวิทย์', 'วรรณโชติ', '', '', '1982-11-06', '', '', '3130700369515.JPG', '2020-10-26', 2, 'IT', 'true'),
(152, '8fa59063019611a19b0548e4091fa609', '0af9fb85cd7df8538381ccf497ed7f11', '3720400688253', '', '', 'นางสาว', 'กรรณิการ์', 'วงษ์บัณฑิต', '', '', '1983-08-08', '', '', '3720400688253.JPG', '2020-10-26', 2, 'OF', 'true'),
(153, '4c25614dba8da8e99659d97e435d8563', '7345251f50ed04ec735183d6d98dda23', '3130700278021', '', '', 'นางสาว', 'พิมพ์นารา', 'นิลฤทธิ์', '', '', '1978-10-21', '', '', '3130700278021.JPG', '2020-10-26', 2, 'BIOT', 'true'),
(154, 'dfe65aa14747a75e4f0c0f3fcba41fc4', '7a6e686c83eb5acc76092f48e845a922', '3141100278811', '', '', 'นางสาว', 'ประทุม', 'พรรณปัญญา', '', '', '1983-10-19', '', '', '3141100278811.JPG', '2020-10-26', 2, 'OF', 'true'),
(155, 'ad260a971062bb1b586b138d298228e0', '5b9d371908b986da59c95ec01a82a32a', '3141100068361', '', '', 'นางสาว', 'วันเพ็ญ', 'อินวอ', '', '', '1983-09-30', '', '', '3141100068361.JPG', '2020-10-26', 2, 'OF', 'true'),
(156, '65211c03db6b01dc854590df20b32e4d', 'f9c02e2f0f7386e9299ffab5dd35e335', '3320100725906', '', '', 'นาง', 'อนัญญา', 'นาคหัสดีย์', '', '', '1982-07-21', '', '', '3320100725906.JPG', '2020-10-26', 2, 'SCC', 'true'),
(157, '61bf1bf89496a621bec4261815788f47', '3c941c86d3a70af327320393055dfa81', '3140400275230', '', '', 'นางสาว', 'ประกายรุ่ง', 'โกมุติบาล', '', '', '1973-03-27', '', '', '3140400275230.JPG', '2020-10-26', 2, 'OHS', 'true'),
(158, '483d04dede32fb4bdee21f54f248f792', '4631e8cd144a96c4fd4bb8cea7c67845', '3302001003228', '', '', 'นางสาว', 'เบ็ญจมาศ', 'นามณรงค์', '', '', '1982-11-16', '', '', '3302001003228.JPG', '2020-10-26', 2, 'OF', 'true'),
(159, '6835ffceba72e19e9a5686a7e17c360c', 'c083e5aa1d8713c15bd5fd1a1e28b6c6', '1199900168612', '', '', 'นาง', 'นริศรา', 'เงินยวง', '', '', '1988-12-15', '', '', '1199900168612.JPG', '2020-10-26', 2, 'SCC', 'true'),
(160, 'e89857c959e5f3596a1a6c34516c6bcd', 'df6518cd16d3a0144504601837c397ea', '1619900094991', '', '', 'นางสาว', 'เยาวนารถ', 'งามนนท์', '', '', '1987-05-06', '', '', '1619900094991.JPG', '2020-10-26', 2, 'SCC', 'true'),
(161, 'ac47fc52e07a662a6461e4dd6d16540a', '9aac15cf181e7b6c0ec9ddc0d91f2d5e', '1251100015996', '', '', 'นางสาว', 'งามเนตร', 'ระพันธ์', '', '', '1985-02-25', '', '', '1251100015996.JPG', '2020-10-26', 2, 'SCC', 'true'),
(162, '94cad1d48e0e6b4e325140876262c310', 'e7a4b1cfc07d9a2a6cebbba354d71c0c', '1350300071103', '', '', 'นาย', 'ธนา', 'ละมะณี', '', '', '1989-02-02', '', '', '1350300071103.JPG', '2020-10-26', 2, 'CS', 'true'),
(163, '7be21c999de6645e622c99663a37aea9', 'bcaa1ee84d7633347201836884fa2572', '3640600059854', '', '', 'นางสาว', 'สุพิณยา', 'อรุณจันทร์', '', '', '1980-04-03', '', '', '3640600059854.JPG', '2020-10-26', 2, 'SCC', 'true'),
(164, '4cb06ab68e136cf4c974bf70c67d1bc4', 'd3091fb052399e31d49e384a57c1cf7f', '1102000357111', '', '', 'นางสาว', 'รัตนาภรณ์', 'พุทธพรทิพย์', '', '', '1985-11-14', '', '', '1102000357111.JPG', '2020-10-26', 2, 'SCC', 'true'),
(165, '30e9e2aa5b42af08791c62507aacb338', 'f66fcfe3fddea61c4bcf66c040875fe2', '1149900399444', '', '', 'นางสาว', 'แพรพลอย', 'พุทธาวงษ์', '', '', '1995-11-22', '', '', '1149900399444.JPG', '2020-10-26', 2, 'OF', 'true'),
(168, 'ec62aad7aa91e0c5361c60879f23fff4', 'a905ae4757e705180e8c24647a8cee4f', '3729800042049', 'อ.', '', 'นาย', 'อุทัย', 'สำรวมจิตร์', 'Uthai', 'Sumruamchit', '1976-03-31', '0894770252', 'uthai.sum@vru.ac.th', '', '1973-03-31', 1, 'DISE', 'true'),
(169, 'cd605ce01257cba367382457513aaa0d', 'f5c7b7d89dce88b6f5c90cee21fdb653', '3101100236116', 'รศ.', 'ดร.', 'นางสาว', 'มนัญญา', 'คำวชิระพิทักษ์', '', '', '1973-09-15', '', 'manunya@vru.ac.th', '', '2021-10-12', 3, 'HE', 'true'),
(170, '71bdcbfa9ea3c2bcc069a25aa84c1b48', '4132dfb919db8880b2d443d46798d41b', '3240500088627', 'ผศ.', '', 'นาง', 'จินตนา', 'จันทร์ศิริ', '', '', '1955-09-30', '', 'chin.tana.1@hotmail.com', '', '2021-10-14', 1, 'MATH', 'true'),
(171, '1a779ed95c20546b765910c8e6e494bc', '6bd077f28d9202db3f729825be60daa0', '3120100575017', 'ผศ.', 'ดร.', 'นาง', 'สำเนียง', 'อภิสันติยาคม', '', '', '1961-04-27', '', 'samneang@vru.ac.th', '', '2021-10-14', 1, 'CHEM', 'true'),
(174, '109fe88a870885dc456ff29379f9d184', '8e4818040566397a680c5054b3577343', '3710500312590', 'อ.', '', 'นาย', 'เอกชัย', 'จงเสรีเจริญ', '', '', '1983-03-16', '', '', '', '2021-10-18', 1, 'PHYS', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `tb_picup`
--

CREATE TABLE `tb_picup` (
  `id_img` int(11) NOT NULL,
  `title_img` text NOT NULL,
  `detail_img` text NOT NULL,
  `major_img` text NOT NULL,
  `date_img` date NOT NULL,
  `place_img` text NOT NULL,
  `cover_img` text NOT NULL,
  `file_img` text NOT NULL,
  `img_type` tinyint(4) NOT NULL,
  `status` enum('true','false') NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_plan`
--

CREATE TABLE `tb_plan` (
  `id_plan` int(11) NOT NULL,
  `titie_plan` varchar(255) NOT NULL,
  `file_plan` varchar(255) NOT NULL,
  `status_plan` enum('true','false') NOT NULL,
  `year` varchar(4) NOT NULL,
  `type_plan` tinyint(4) NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ safetyuni`
--

CREATE TABLE `tb_ safetyuni` (
  `id_uni` int(11) NOT NULL,
  `name_uni` varchar(255) NOT NULL,
  `link_uni` varchar(255) NOT NULL,
  `file_uni` varchar(255) NOT NULL,
  `type_uni` tinyint(4) NOT NULL,
  `status_uni` int(11) NOT NULL,
  `create_at` date NOT NULL,
  `update_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' Safetyuni';

-- --------------------------------------------------------

--
-- Table structure for table `tb_scoures`
--

CREATE TABLE `tb_scoures` (
  `id_scoures` int(11) NOT NULL,
  `name_scoures` varchar(255) NOT NULL,
  `link_scoures` varchar(255) NOT NULL,
  `img_scoures` varchar(255) NOT NULL,
  `detail_scoures` text NOT NULL,
  `status_scoures` enum('true','false') NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `create_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_youtube`
--

CREATE TABLE `tb_youtube` (
  `id_youtube` int(11) NOT NULL,
  `title_youtube` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube_code` text COLLATE utf8_unicode_ci NOT NULL,
  `date_youtube` date NOT NULL,
  `link_youtube` text COLLATE utf8_unicode_ci NOT NULL,
  `major_youtube` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='สื่อวิดีโอเผยแพร่';

--
-- Dumping data for table `tb_youtube`
--

INSERT INTO `tb_youtube` (`id_youtube`, `title_youtube`, `youtube_code`, `date_youtube`, `link_youtube`, `major_youtube`) VALUES
(1, 'Tip DIY by yourself EP.1 การทำหน้ากากอนามัยแบบผ้า', 'pEC84WcyjPE', '2020-04-25', 'https://youtu.be/pEC84WcyjPE', 'OF'),
(2, 'Tip DIY by yourself EP.2 การทำเจลแอลกอฮอล์ล้างมือแบบง่ายๆ', 'u3TNMr-ukIE', '2020-04-27', 'https://youtu.be/u3TNMr-ukIE', 'OF'),
(3, 'Science in everyday life EP.1 ปลูกทุเรียนลดโลกร้อน', 'iXoACBOcUiY', '2020-05-07', 'https://youtu.be/iXoACBOcUiY', 'OF'),
(5, 'Tip Tutorial EP.1 การใช้โปรแกรม Zoom', 'luNPa_2fnIA', '2020-07-13', 'https://youtu.be/luNPa_2fnIA', 'OF'),
(6, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรนวัตกรรมดิจิทัลและวิศวกรรมซอฟต์แวร์', 'qUoi7CZ9nVg', '2020-08-24', 'https://youtu.be/qUoi7CZ9nVg ', 'OF'),
(8, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรการจัดการภัยพิบัติและบรรเทาสาธารณภัย', 'MBBuBOB9ngc', '2020-08-24', 'https://youtu.be/MBBuBOB9ngc', 'OF'),
(9, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรโภชนาการและการกำหนดอาหาร', 'WF11r89MOjM', '2020-08-24', 'https://youtu.be/WF11r89MOjM', 'OF'),
(11, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรวิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม', 'NzIIovr3-NU', '2020-08-24', 'https://youtu.be/NzIIovr3-NU', 'OF'),
(12, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรอาชีวอนามัยและความปลอดภัย', 'LbdPH4JPdQs', '2020-08-24', 'https://youtu.be/LbdPH4JPdQs', 'OF'),
(13, 'การแสดงด้านศิลปวัฒนธรรม หลักสูตรเทคโนโลยีสารสนเทศ', 'wH5FvBWGVX0', '2020-08-24', 'https://youtu.be/wH5FvBWGVX0', 'OF'),
(15, 'แนะนำหลักสูตรนวัตกรรมอาหารและเครื่องดื่มเพื่อสุขภาพ', 'iSVasc97dsI', '2020-08-24', 'https://youtu.be/iSVasc97dsI', 'OF'),
(16, 'แนะนำหลักสูตรคหกรรมศาสตร์', 'CT4gEG8kJAs', '2020-08-24', 'https://youtu.be/CT4gEG8kJAs', 'OF'),
(17, 'แนะนำหลักสูตรการจัดการภัยพิบัติและบรรเทาสาธารณภัย', '2i5Yf1J-4i4', '2020-08-24', 'https://youtu.be/2i5Yf1J-4i4', 'OF'),
(18, 'แนะนำหลักสูตรโภชนาการและการกำหนดอาหาร', '2i5Yf1J-4i4', '2020-08-24', 'https://youtu.be/2i5Yf1J-4i4', 'OF'),
(19, 'แนะนำหลักสูตรวิทยาศาสตร์และเทคโนโลยีสิ่งแวดล้อม', 'wCUWnM_-F6I', '2020-08-24', 'https://youtu.be/wCUWnM_-F6I', 'OF'),
(24, 'เมนูสุขภาพ สลัดผักจมูกข้าวไรซ์เบอร์รี่', 'TpnlM8vz_oM', '2020-06-11', 'https://youtu.be/TpnlM8vz_oM', 'OF'),
(21, 'แนะนำหลักสูตรเทคโนโลยีสารสนเทศ', 'wYpLcNEQIgs', '2020-08-24', 'https://youtu.be/wYpLcNEQIgs', 'OF'),
(23, 'กิจกรรมจิตอาสา "ทำความดี ด้วยหัวใจ" ประจำเดือน สิงหาคม 2562', '-MewgnNuCuE', '2019-08-07', 'https://youtu.be/-MewgnNuCuE', 'OF'),
(25, 'เมนูสุขภาพ ชานมไข่มุกจมูกข้าวไรซ์เบอร์รี่', 'gTUx-lwGPes', '2020-06-12', 'https://youtu.be/gTUx-lwGPes', 'OF'),
(22, 'การฝึกประสบการณ์วิชาชีพด้านโภชนาการ ที่ สปป.ลาว ปี2562', 'TIFecuqHLaM', '2019-07-30', 'https://youtu.be/TIFecuqHLaM', 'OF'),
(26, 'Tip DIY by yourself EP.3 ทำง่าย ขายดี ด้วยน้ำขวดพาสเจอร์ไรส์', 'xBgqlHxEDzI', '2021-08-19', 'https://youtu.be/xBgqlHxEDzI', 'OF'),
(27, 'Tip DIY by yourself EP.4 เรียน ๆ เล่น ๆ กับสีเต้นระบำ', 'oCM7oJYtOTw', '2021-08-19', 'https://youtu.be/oCM7oJYtOTw', 'OF'),
(28, 'Tip DIY by yourself EP.5 น้ำสีรุ้ง', 'eyfnyzznIDw', '2021-08-19', 'https://youtu.be/eyfnyzznIDw', 'OF'),
(29, 'Tip DIY by yourself EP.6 เรียนรู้วิทยาศาสตร์ผ่านอาหาร 1 จาน', 'x2bcIKo-aSI', '2021-08-19', 'https://youtu.be/x2bcIKo-aSI', 'OF'),
(30, 'Tip DIY by yourself EP.7 แก้วน้ำวิเศษ', 'Q9-Dx9Oso-w', '2021-08-19', 'https://youtu.be/Q9-Dx9Oso-w', 'OF'),
(31, 'Tip DIY by yourself EP.8 ปลูกตามใจทำได้เองที่บ้าน', 'LuitoCwDcts', '2021-08-19', 'https://youtu.be/LuitoCwDcts', 'OF'),
(32, 'Tip DIY by yourself EP.9 ยีสต์…จุลินทรีย์ดีๆ มีประโยชน์', '7Iml_rH23qo', '2021-08-19', 'https://youtu.be/7Iml_rH23qo', 'OF'),
(33, 'Tip DIY by yourself EP.10 How to ปั้นน้ำเป็นตัว', 'W2yeN2eFAqc', '2021-08-19', 'https://youtu.be/W2yeN2eFAqc', 'OF'),
(34, 'Tip DIY by yourself EP.11 หมึกกรอบทำง่าย ทำได้ที่บ้าน', 'pLpWHw1CGj0', '2021-08-19', 'https://youtu.be/pLpWHw1CGj0', 'OF'),
(35, 'Tip DIY by yourself EP.12 การผลิตนมเปรี้ยวพร้อมดื่มข้าวไรซ์เบอรี่', 'CfI_eIDIZiY', '2021-08-19', 'https://youtu.be/CfI_eIDIZiY', 'OF'),
(36, 'Tip DIY by yourself EP.13 อิตาเลี่ยนโซดา แยกชั้น ซาบซ่า', 'm_FO89RgU5k', '2021-08-19', 'https://youtu.be/m_FO89RgU5k', 'OF'),
(37, 'Tip DIY by yourself EP.14 การแยกขยายพันธุ์รังเลี้ยงผึ้งชันโรงขนเงิน', '1J3odNItRcw', '2021-08-19', 'https://youtu.be/1J3odNItRcw', 'OF');

-- --------------------------------------------------------

--
-- Table structure for table `userlogs`
--

CREATE TABLE `userlogs` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userlogs`
--

INSERT INTO `userlogs` (`id`, `ip`, `device`, `os`, `browser`, `page`, `created_at`) VALUES
(1, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 09:56:41'),
(2, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 10:27:12'),
(3, '::1', 'Computer', 'Windows 10', 'Chrome', 'contact', '2021-12-20 10:27:20'),
(4, '::1', 'Computer', 'Windows 10', 'Chrome', 'about', '2021-12-20 10:27:24'),
(5, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 10:27:26'),
(6, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 11:11:52'),
(7, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 13:29:49'),
(8, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 14:43:00'),
(9, '::1', 'Computer', 'Windows 10', 'Chrome', 'form_personal', '2021-12-20 14:43:05'),
(10, '::1', 'Computer', 'Windows 10', 'Chrome', 'form_personal', '2021-12-20 14:44:21'),
(11, '::1', 'Computer', 'Windows 10', 'Chrome', 'form_personal', '2021-12-20 14:55:12'),
(12, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 15:17:15'),
(13, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 15:17:53'),
(14, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 15:18:26'),
(15, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 15:18:50'),
(16, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:27:13'),
(17, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:27:14'),
(18, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 15:31:23'),
(19, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:57:15'),
(20, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:57:17'),
(21, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:57:18'),
(22, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:57:19'),
(23, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:58:11'),
(24, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:58:42'),
(25, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-20 15:59:01'),
(26, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog-all', '2021-12-20 15:59:06'),
(27, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-20 16:17:30'),
(28, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-21 09:05:46'),
(29, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-21 09:06:45'),
(30, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 08:42:48'),
(31, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-22 08:43:17'),
(32, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 09:10:29'),
(33, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 09:11:39'),
(34, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-22 09:11:42'),
(35, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 11:51:19'),
(36, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 12:45:07'),
(37, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 13:12:48'),
(38, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-22 13:17:32'),
(39, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-22 13:17:35'),
(40, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-24 09:08:43'),
(41, '::1', 'Computer', 'Windows 10', 'Chrome', 'about', '2021-12-24 09:10:58'),
(42, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-24 09:11:26'),
(43, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-27 12:39:53'),
(44, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-27 12:39:59'),
(45, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-28 09:25:31'),
(46, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-28 09:25:46'),
(47, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-28 11:01:58'),
(48, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-28 11:02:00'),
(49, '::1', 'Computer', 'Windows 10', 'Chrome', 'blog', '2021-12-28 11:04:06'),
(50, '::1', 'Computer', 'Windows 10', 'Chrome', 'index', '2021-12-28 11:04:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminlogs`
--
ALTER TABLE `adminlogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_banner`
--
ALTER TABLE `tb_banner`
  ADD PRIMARY KEY (`id_ban`);

--
-- Indexes for table `tb_files`
--
ALTER TABLE `tb_files`
  ADD PRIMARY KEY (`id_files`);

--
-- Indexes for table `tb_gf`
--
ALTER TABLE `tb_gf`
  ADD PRIMARY KEY (`id_gf`);

--
-- Indexes for table `tb_major`
--
ALTER TABLE `tb_major`
  ADD PRIMARY KEY (`id_major`);

--
-- Indexes for table `tb_news`
--
ALTER TABLE `tb_news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `tb_per`
--
ALTER TABLE `tb_per`
  ADD PRIMARY KEY (`id_per`);

--
-- Indexes for table `tb_picup`
--
ALTER TABLE `tb_picup`
  ADD PRIMARY KEY (`id_img`);

--
-- Indexes for table `tb_plan`
--
ALTER TABLE `tb_plan`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indexes for table `tb_ safetyuni`
--
ALTER TABLE `tb_ safetyuni`
  ADD PRIMARY KEY (`id_uni`);

--
-- Indexes for table `tb_scoures`
--
ALTER TABLE `tb_scoures`
  ADD PRIMARY KEY (`id_scoures`);

--
-- Indexes for table `tb_youtube`
--
ALTER TABLE `tb_youtube`
  ADD PRIMARY KEY (`id_youtube`);

--
-- Indexes for table `userlogs`
--
ALTER TABLE `userlogs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `adminlogs`
--
ALTER TABLE `adminlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banner`
--
ALTER TABLE `tb_banner`
  MODIFY `id_ban` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสbanner';
--
-- AUTO_INCREMENT for table `tb_files`
--
ALTER TABLE `tb_files`
  MODIFY `id_files` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_gf`
--
ALTER TABLE `tb_gf`
  MODIFY `id_gf` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_major`
--
ALTER TABLE `tb_major`
  MODIFY `id_major` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_per`
--
ALTER TABLE `tb_per`
  MODIFY `id_per` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสพนักงาน', AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT for table `tb_picup`
--
ALTER TABLE `tb_picup`
  MODIFY `id_img` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_plan`
--
ALTER TABLE `tb_plan`
  MODIFY `id_plan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_ safetyuni`
--
ALTER TABLE `tb_ safetyuni`
  MODIFY `id_uni` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_scoures`
--
ALTER TABLE `tb_scoures`
  MODIFY `id_scoures` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_youtube`
--
ALTER TABLE `tb_youtube`
  MODIFY `id_youtube` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `userlogs`
--
ALTER TABLE `userlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
