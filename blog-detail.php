<?php

    require_once('php/connect.php');
    $no = 0;
    $sql = "SELECT * FROM tb_picup WHERE cover_img ='".$_GET['id']."'";
    $result = $conn->query($sql) or die($conn->error);
    
    if($result->num_rows > 0){

        $row = $result->fetch_assoc();

    } else {

        header('Loacation: blog.php');
    
    }


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="9PR-_y1nMZeXzVEDJgarF2uOc0qzhM1JBMVILFCl5nA" />
    <!-- ทำให้บนมือถือซูมไม่ได้ (เฉพาะบางเบราเซอร์)-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $row['title_img'] ?></title>
    <!-- Search Engine -->
    <meta name="description" content="<?php echo $row['title_img'] ?>">
    <meta name="image"
    content="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['cover_img']?>">
    <meta name="keywords"
        content="คณะวิทย์ฯ, scitech, sci,vru,techno, information, computer, physics, chemistry, bio,ราชการ, สมัครงาน, joomla, จัดซื้อจัดจ้าง, บันทึกข้อความ, เรียนต่อ, โควตา, รับตรง, ภาคี,เทคโน, เทคคอม, มัลติ, มัลติมีเดีย, มีเดีย, การจัดการคอม, ห้องเรียนคอม, เฟรชชี่, น้องใหม่, นักศึกษา, วิทยาศาสตร์, สำนักงานคณะวิทย์, วิทย์, วิทยฯ,มหาวิทยาลัยราชภัฏ, ศูนย์วิทย์, ฟิสิกส์, ฟิสิก, ชีววะ, เคมี, สิ่งแวดล้อม, เทคโนโลยีสารสนเทศ, มหาวิทยาลัย,ราชภัฏ,วไลยอลงกรณ์,ในพระบรมราชูปถัมภ์,ปริญญาตรี,ปริญญาโท,เรียนต่อปริญญาโท,เรียนต่อปริญญาเอก"> 
    <meta name="robots" content="index, nofollow">
    <meta name="author" content="ScitechVRU">
    <!-- Schema.org for Google -->
    <meta name="name" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="description" content="<?php echo $row['title_img'] ?>">
    <meta name="image"
    content="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['cover_img']?>">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta property="fb:app_id" content="620851126015605">
    <meta property="og:title" content="<?php echo $row['title_img'] ?>">
    <meta property="og:description" content="<?php echo $row['title_img'] ?>">
    <meta property="og:image"
    content="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['cover_img']?>">
    <meta property="og:site_name" content="https://sci.vru.ac.th/ScitechVRU/">
    <meta property="og:type" content="website">
    <!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- CSS img gallery Ekko Lightbox-->
    <link rel="stylesheet" href="node_modules/ekko-lightbox/dist/ekko-lightbox.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <script type='text/javascript'
        src='https://platform-api.sharethis.com/js/sharethis.js#property=61b050750b7995001f194920&product=inline-share-buttons'
        async='async'></script>
</head>

<body>

    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php');?>
    <!-- End Section Navbar -->

    <!-- Section Page-title -->

    <header class="jarallax" data-jarallax='{ "speed": 0.6 }'
        style="background-image: url(http://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['cover_img']?>); ">

        <div class="page-image">
            <h1 class="display-4 font-weight-bold"><?php echo $row['title_img'] ?></h1>
            <p class="lead"><?php echo $row['place_img'] ?></p>
        </div>

    </header>

    <!-- Section Blog Detail -->
    <section class="container blog-content">
        <div class="row">
            <div class="col-12">
                <?php echo $row['detail_img'] ?>
                    <div class="card">
                        
                        <div class="row">
                        <?php while ($row = $result->fetch_assoc()){ 
                            $no++;
                        ?>
                            <div class="col-md-4">
                                <a href="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['file_img']?>" data-toggle="lightbox" data-title="<?php echo $row['title_img'] ?>" data-gallery="gallery">
                                    <img class="img-fluid mb-2" src="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row['file_img']?>" alt="<?php echo $row['title_img'] ?>">
                                </a>
                            </div>
                            <?php }?>
                        </div>
                        
                    </div>
            </div>
            <div class="col-12">
                <hr>
                <p class="text-right text-muted"><?php echo date_format(new DateTime($row['date_img']),"j F Y"); ?>
                </p>
            </div>
            <div class="col-12">
                <div class="owl-carousel owl-theme">
                    <?php 
                        $sql_RAND = "SELECT * FROM tb_picup WHERE major_img='OF' order by RAND() LIMIT 6";
                        $result_RAND = $conn->query($sql_RAND) or die($conn->error);

                        while($row_RAND = $result_RAND->fetch_assoc()){ 
                    ?>
                    <section class="col-12 p-2">
                        <div class="card h-100">
                            <a href="blog-detail.php?id=<?php echo $row_RAND['cover_img']?>" class="warpper-card-img">
                                <img class="card-img-top" src="https://sci.vru.ac.th/ScitechVRU/assets/images/activity/<?php echo $row_RAND['cover_img']?>" alt="<?php echo $row_RAND['title_img'] ?>">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $row_RAND['title_img'] ?></h5>
                                <p class="card-text"><?php echo date_format(new DateTime($row_RAND['date_img']),"j F Y");?></p>
                            </div>
                            <div class="p-3">
                                <a href="blog-detail.php?id=<?php echo $row_RAND['cover_img']?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                            </div>
                        </div>
                    </section>
                <?php }?>
                </div>
            </div>
            <div class="col-12 ">
                <!-- ShareThis BEGIN -->
                <div class="sharethis-inline-follow-buttons py-3"></div>
                <!-- ShareThis END -->
                <div class="sharethis-inline-share-buttons py-3"></div>
            </div>
            <div class="col-12">
                <div class="fb-comments"
                    data-href="https://sci.vru.ac.th/ScitechVRU/blog-detail.php?id_scitechvru=<?php echo $row['cover_img']?>"
                    data-width="100%" data-numposts="5"></div>
                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous"
                    src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v12.0&appId=1304852752948340&autoLogAppEvents=1"
                    nonce="M7uiJSAn"></script>
            </div>
        </div>

        </div>
    </section>
    <!-- Section About -->
    <section class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/1.jpg); ">
        <div class="page-image">

            <img class="img-fluid" src="assets/image/logo.png" width="150" alt=" ">
            <h2 class="text-white display-4 font-weight-bold ">Scitech VRU</h2>
            <div class="star-rating">
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <span>☆</span>
                <div class="star-current" style="width:100%;">
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                </div>
            </div>

        </div>
    </section>

    <!-- Section footer -->
    <?php include_once('includes/footer.php');?>
    <?php include_once('php/userlogs.php') ?>
    <!-- End Section footer -->
    <!-- Your ปลั๊กอินแชท code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <!-- Section On to Top -->
    <div class="to-top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <!-- CSS img gallery Ekko Lightbox -->
    <script src="node_modules/ekko-lightbox/dist/ekko-lightbox.min.js"></script>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap">
    </script>
    <script src="assets/js/main.js "></script>
    <script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "1739708992734392");
    chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    dots: false
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    });
      
        //              DO NOT IMPLEMENT                //
        //       this code through the following        //
        //                                              //
        //   Floodlight Pixel Manager                   //
        //   DCM Pixel Manager                          //
        //   Any system that places code in an iframe   //
        (function () {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
            + '.po.st/static/v4/post-widget.js#publisherKey=556iuqt4139l475oo1e8';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
        /*  Page specific script  */
        $(function () {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true
        });
        });

        $('.filter-container').filterizr({gutterPixels: 3});
        $('.btn[data-filter]').on('click', function() {
        $('.btn[data-filter]').removeClass('active');
        $(this).addClass('active');
        });
    })
    </script>
    

</body>

</html>