<?php

    require_once('php/connect.php');
    $id = $_GET['id'];
  
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>ScitechVRU FormPersonal</title>
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="9PR-_y1nMZeXzVEDJgarF2uOc0qzhM1JBMVILFCl5nA" />
    <!-- ทำให้บนมือถือซูมไม่ได้ (เฉพาะบางเบราเซอร์)-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Search Engine -->
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="keywords"
        content="คณะวิทย์ฯ, scitech, sci,vru,techno, information, computer, physics, chemistry, bio,ราชการ, สมัครงาน, joomla, จัดซื้อจัดจ้าง, บันทึกข้อความ, เรียนต่อ, โควตา, รับตรง, ภาคี,เทคโน, เทคคอม, มัลติ, มัลติมีเดีย, มีเดีย, การจัดการคอม, ห้องเรียนคอม, เฟรชชี่, น้องใหม่, นักศึกษา, วิทยาศาสตร์, สำนักงานคณะวิทย์, วิทย์, วิทยฯ,มหาวิทยาลัยราชภัฏ, ศูนย์วิทย์, ฟิสิกส์, ฟิสิก, ชีววะ, เคมี, สิ่งแวดล้อม, เทคโนโลยีสารสนเทศ, มหาวิทยาลัย,ราชภัฏ,วไลยอลงกรณ์,ในพระบรมราชูปถัมภ์,ปริญญาตรี,ปริญญาโท,เรียนต่อปริญญาโท,เรียนต่อปริญญาเอก">
    <meta name="robots" content="index, nofollow">
    <meta name="language" content="English">
    <meta name="author" content="ScitechVRU">
    <!-- Schema.org for Google -->
    <meta name="name" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="fb:app_id" content="620851126015605">
    <meta name="og:title" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="og:description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="og:image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="og:url" content="http://sci.vru.ac.th/">
    <meta name="og:type" content="website">
</head>

<body>

    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php');?>
    <!-- End Section Navbar -->

    <!-- Section Page-title -->

    <header class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/8.jpg); ">
        <div class="page-image ">

            <h1 class="display-4 font-weight-bold"><?php switch ($id) {
                  case 'stu': echo "แบบฟอร์มนักศึกษา";
                        break;
                  case 'man': echo "คู่มือนักศึกษา";
                        break;
                  case 'per': echo "แบบฟอร์มอาจารย์/เจ้าหน้าที่";
                        break;
                  case 'res': echo "แบบฟอร์มวิจัย";
                        break;
                  case 'info': echo "สารสนเทศ";
                        break;
                  case 'reg': echo "ระเบียบ/ข้อบังคับ/กฎหมายที่เกี่ยวข้อง";
                        break;
                  case 'qua': echo "ประกันคุณภาพ";
                        break;
                  case 'mua': echo "คู่มือ";
                        break;
                  case 'jr1': echo "ระเบียบการตีพิมพ์ JRIST";
                        break;
                  case 'jr2': echo "รูปแบบบทความวิชาการ JRIST";
                        break;
                  case 'jr3': echo "รูปแบบบทความวิจัย JRIST";
                        break;
                  case 'jr4': echo "ใบนำส่งบทความฯ JRIST";
                        break;
                       
                  }; ?></h1>
            <p class="lead">คณะวิทยาศาสตร์และเทคโนโลยี</p>

        </div>
    </header>

    <section class="container py-5">
        <div class="row pb-4">
            <?php 
            $num = 0;
            $sql = "SELECT * FROM tb_files WHERE type_files='".$id."' AND status_files='true'";
            $result = $conn->query($sql) or die($conn->error);
   
            ?>
            <section class="col-12 col-sm-6 col-md-12 p-2">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">ลำดับ</th>
                                <th scope="col">ชื่อไฟล์</th>
                                <th scope="col">ดาวน์โหลดแบบฟอร์ม</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php   if ($result->num_rows > 0){
                                    while ($row = $result->fetch_assoc()){
                                        $num++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $num;?></th>
                                <td><?php echo $row['title_files'];?></td>
                                <?php if($row['link_files']){?>
                                <td><a href="<?php echo $row['link_files'] ?>"
                                        class="btn btn-primary btn-block">ดาวน์โหลดแบบฟอร์ม</a></td>
                                <?php } else {?>
                                <td><a href="https://sci.vru.ac.th/ScitechVRU/assets/files/allfiles/<?php echo $row['title_files']?>"
                                        class="btn btn-primary btn-block">ดาวน์โหลดแบบฟอร์ม</a></td>
                                <?php }?>
                            </tr>
                            <?php 
                                        }
                                } //end loop
                            ?>
                        </tbody>
                    </table>
                    </table>
                </div>
            </section>


        </div>


    </section>


    <!-- Section footer -->
    <?php include_once('includes/footer.php');?>
    <?php include_once('php/userlogs.php') ?>
    <!-- End Section footer -->
    <!-- Your ปลั๊กอินแชท code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <!-- Section On to Top -->
    <div class="to-top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap">
    </script>
    <script src="assets/js/main.js "></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "1739708992734392");
    chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    $(function() {
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
    </script>


</body>

</html>