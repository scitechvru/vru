<?php

    require_once('php/connect.php');
    require_once('php/array.php');

    $id = $_GET['id'];
    $title = isset($_GET['title']) ? $_GET['title'] : '1';

    $sql1 = "SELECT * FROM `report_title` WHERE id_title ='".$id."'";
    $result1 = $conn->query($sql1);
    $row1 = $result1->fetch_assoc();
    $Date = explode("-", $row1['report_date']);
    $datethai = $Date[2] . " " . $thaimonth[$Date[1]] . " " . $Date[0];

    if (!$result1) {
        header('Location: index.php');
    }



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>ScitechVRU Report</title>
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="9PR-_y1nMZeXzVEDJgarF2uOc0qzhM1JBMVILFCl5nA" />
    <!-- ทำให้บนมือถือซูมไม่ได้ (เฉพาะบางเบราเซอร์)-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!-- CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Search Engine -->
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="keywords"
        content="คณะวิทย์ฯ, scitech, sci,vru,techno, information, computer, physics, chemistry, bio,ราชการ, สมัครงาน, joomla, จัดซื้อจัดจ้าง, บันทึกข้อความ, เรียนต่อ, โควตา, รับตรง, ภาคี,เทคโน, เทคคอม, มัลติ, มัลติมีเดีย, มีเดีย, การจัดการคอม, ห้องเรียนคอม, เฟรชชี่, น้องใหม่, นักศึกษา, วิทยาศาสตร์, สำนักงานคณะวิทย์, วิทย์, วิทยฯ,มหาวิทยาลัยราชภัฏ, ศูนย์วิทย์, ฟิสิกส์, ฟิสิก, ชีววะ, เคมี, สิ่งแวดล้อม, เทคโนโลยีสารสนเทศ, มหาวิทยาลัย,ราชภัฏ,วไลยอลงกรณ์,ในพระบรมราชูปถัมภ์,ปริญญาตรี,ปริญญาโท,เรียนต่อปริญญาโท,เรียนต่อปริญญาเอก">
    <meta name="robots" content="index, nofollow">
    <meta name="language" content="English">
    <meta name="author" content="ScitechVRU">
    <!-- Schema.org for Google -->
    <meta name="name" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="fb:app_id" content="620851126015605">
    <meta name="og:title" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="og:description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="og:image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="og:url" content="http://sci.vru.ac.th/">
    <meta name="og:type" content="website">
</head>

<body>

    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php');?>
    <!-- End Section Navbar -->

    <!-- Section Page-title -->

    <header class="jarallax" data-jarallax='{ "speed": 0.6 }' style="background-image: url( assets/image/img/8.jpg); ">
        <div class="page-image ">

            <h1 class="display-4 font-weight-bold">
                <?php switch($row1['report_type']){
                                      case 1: echo "ระเบียบวาระการประชุมคณะกรรมการวิชาการคณะ"; 
                                            break;
                                      case 2: echo "ระเบียบวาระการประชุมคณะกรรมการบริหารคณะ"; 
                                            break;
                                      case 3: echo "คณบดีสัญจร"; 
                                            break;
                                    };?>
            </h1><br>
            <p class="lead">คณะวิทยาศาสตร์และเทคโนโลยี<br>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์
                จังหวัดปทุมธานี</p><br>
            <p class="lead">ครั้งที่ <?php echo $row1['report_time']?><br>ในวันที่
                <?php echo $datethai;?> <br>สถานที่
                <?php echo $row1['location']?></p>


        </div>
    </header>

    <section class="container py-5">
        <div class="row pb-4 ">
            <div class="col">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    ระเบียบที่ 1 เรื่องประธานแจ้งให้ทราบ
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql1 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '1' ";
                                $result1 = $conn->query($sql1);            
                                    if ($result1->num_rows > 0){
                                    while($row1 = $result1->fetch_assoc()) { 
                                ?>
                                <?php if ($row1['link']) {?>
                                <a HREF="<?php echo $row1['link'];?>"
                                    target='_blank'><?php echo $row1['name_title'];?></a><br>
                                <?php }elseif ($row1['file']){?>
                                <a HREF="assets/files/report/<?php echo $row1['file'];?>"
                                    target='_blank'><?php echo $row1['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row1['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                    aria-controls="collapseTwo">
                                    ระเบียบที่ 2 เรื่องรับรองรายงานการประชุม
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                            data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql2 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '2' ";
                                $result2 = $conn->query($sql2);            
                                    if ($result2->num_rows > 0){
                                    while($row2 = $result2->fetch_assoc()) { 
                                ?>
                                <?php if ($row2['link']) {?>
                                <a HREF="<?php echo $row2['link'];?>"
                                    target='_blank'><?php echo $row2['name_title'];?></a><br>
                                <?php }elseif ($row2['file']){?>
                                <a HREF="assets/files/report/<?php echo $row2['file'];?>"
                                    target='_blank'><?php echo $row2['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row2['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">
                                    ระเบียบที่ 3 เรื่องสืบเนือง
                                </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql3 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '3' ";
                                $result3 = $conn->query($sql3);            
                                    if ($result3->num_rows > 0){
                                    while($row3 = $result3->fetch_assoc()) { 
                                ?>
                                <?php if ($row3['link']) {?>
                                <a HREF="<?php echo $row3['link'];?>"
                                    target='_blank'><?php echo $row3['name_title'];?></a><br>
                                <?php }elseif ($row3['file']){?>
                                <a HREF="assets/files/report/<?php echo $row3['file'];?>"
                                    target='_blank'><?php echo $row3['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row3['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapse4" aria-expanded="false"
                                    aria-controls="collapse4">
                                    ระเบียบที่ 4 เรื่องเสนอเพื่อทราบ
                                </button>
                            </h2>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql4 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '4' ";
                                $result4 = $conn->query($sql4);            
                                    if ($result4->num_rows > 0){
                                    while($row4 = $result4->fetch_assoc()) { 
                                ?>
                                <?php if ($row4['link']) {?>
                                <a HREF="<?php echo $row4['link'];?>"
                                    target='_blank'><?php echo $row4['name_title'];?></a><br>
                                <?php }elseif ($row4['file']){?>
                                <a HREF="assets/files/report/<?php echo $row4['file'];?>"
                                    target='_blank'><?php echo $row4['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row4['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapse5" aria-expanded="false"
                                    aria-controls="collapse5">
                                    ระเบียบที่ 5 เรื่องเสนอเพื่อพิจารณา
                                </button>
                            </h2>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql5 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '5' ";
                                $result5 = $conn->query($sql5);            
                                    if ($result5->num_rows > 0){
                                    while($row5 = $result5->fetch_assoc()) { 
                                ?>
                                <?php if ($row5['link']) {?>
                                <a HREF="<?php echo $row5['link'];?>"
                                    target='_blank'><?php echo $row5['name_title'];?></a><br>
                                <?php }elseif ($row5['file']){?>
                                <a HREF="assets/files/report/<?php echo $row5['file'];?>"
                                    target='_blank'><?php echo $row5['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row5['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button"
                                    data-toggle="collapse" data-target="#collapse6" aria-expanded="false"
                                    aria-controls="collapse6">
                                    ระเบียบที่ 6 เรื่องอื่นๆ
                                </button>
                            </h2>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
                            <div class="card-body">
                                <?php 
                                $sql6 = "SELECT * FROM report_detail WHERE id_report = '".$id."' AND title = '6' ";
                                $result6 = $conn->query($sql6);            
                                    if ($result6->num_rows > 0){
                                    while($row6 = $result6->fetch_assoc()) { 
                                ?>
                                <?php if ($row6['link']) {?>
                                <a HREF="<?php echo $row6['link'];?>"
                                    target='_blank'><?php echo $row6['name_title'];?></a><br>
                                <?php }elseif ($row6['file']){?>
                                <a HREF="assets/files/report/<?php echo $row6['file'];?>"
                                    target='_blank'><?php echo $row6['name_title'];?></a><br>
                                <?php }else{?>
                                <p class="text-center"><?php echo $row6['name_title'];?></p><br>
                                <?php } ?>
                                <?php } }else{?>
                                <p class="text-center">ไม่มีข้อมูล</p>
                                <?php }?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Section footer -->
    <?php include_once('includes/footer.php');?>
    <?php include_once('php/userlogs.php') ?>
    <!-- End Section footer -->
    <!-- Your ปลั๊กอินแชท code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>
    <!-- Section On to Top -->
    <div class="to-top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap">
    </script>
    <script src="assets/js/main.js "></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "1739708992734392");
    chatbox.setAttribute("attribution", "biz_inbox");
    </script>
    <!-- Your SDK code -->
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>


</body>

</html>