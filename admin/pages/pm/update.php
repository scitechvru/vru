<?php include_once('../authen.php') ?>
<?php

if(isset($_POST['submit'])){

    $image_name = $_POST['data_file'];
    $status = isset($_POST['status']) ? 'true' : 'false' ;
        
    $CardId = explode("-",$_POST['idcard_per']);
    $idcard_per = $CardId[0].$CardId[1].$CardId[2].$CardId[3].$CardId[4]; // ตัด- บัตรประชาชน
    $BirthDate = explode("-",$_POST['bdate_per']);
    $bdate_per = $BirthDate[2].$BirthDate[1].$BirthDate[0]; //ตัด - วันเกิด  เรียงวันเกิดใหม่
    $TelMo = explode("-",$_POST['phone_per']);
    $phone_per = $TelMo[0].$TelMo[1].$TelMo[2]; // ตัด - เบอร์โทรศัพท์

    $username = $idcard_per;
    $password = password_hash($bdate_per,PASSWORD_DEFAULT); //เข้ารหัส hash
    if($_FILES['file']['name'] != ''){
    
    $file = $_FILES['file']['tmp_name']; // ไฟล์รูปภาพ
    $sourceProperties = getimagesize($file); // ตรวจสอบไฟล์
    $folderPath = "../../../assets/images/people/"; // นำไฟล์ไปเก็บไว้ที่
    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION); //นำเอานามสกุลไฟล์มาใช้งาน
    $image_name = $idcard_per.'_edit'.round(microtime(true)*9999); //เปลี่ยนชื่อไฟล์ใหม่
    $imageType = $sourceProperties[2]; // แยกประเภทนามสกุล

    switch ($imageType) {

        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagepng($targetLayer,$folderPath. $image_name. ".". $ext);
            break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($targetLayer,$folderPath. $image_name. ".". $ext);
                break;

        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagejpeg($targetLayer,$folderPath. $image_name. ".". $ext);
            break;

        default:
            echo "Invalid Image type.";
            exit;
            break;
        }
    
        if(move_uploaded_file($file,$folderPath.$image_name.'.'.$ext)){
            $image_delete = ROOT_PATH . $folderPath . pathinfo($_POST['data_file'], PATHINFO_BASENAME);
                unlink($image_delete);
            }else{
                echo '<script> alert("ไม่สามารถอัพโหลดรูปภาพใหม่ได้ โปรดลองอีกครั้ง")</script>'; 
                header('Refresh:0; url=index.php'); 
            }
    }
            $sql = "UPDATE `tb_per` 
            SET user_per = '".$username."', 
                pass_per = '".$password."', 
                idcard_per = '".$idcard_per."', 
                tname2_per = '".$_POST['tname2_per']."', 
                tname1_per = '".$_POST['tname1_per']."',  
                tname_per = '".$_POST['tname_per']."', 
                fnamet_per = '".$_POST['fnamet_per']."', 
                lnamet_per = '".$_POST['lnamet_per']."', 
                fnamee_per = '".$_POST['fnamee_per']."', 
                lnamee_per = '".$_POST['lnamee_per']."', 
                bdate_per = '".$_POST['bdate_per']."', 
                phone_per = '".$phone_per."', 
                email_per = '".$_POST['email_per']."', 
                img_per = '".$image_name.'.'. $ext."', 
                dates_per = '".date("Y-m-d H:i:s")."', 
                type_per = '".$_POST['type_per']."', 
                major_per = '".$_POST['major_per']."'
                WHERE id_per = '".$_POST['id']."'";
        $result = $conn->query($sql);
        if($result){
        echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
        header('Refresh:0; url=index.php');
        }else{
        echo '<script> alert("Error Creating!")</script>'; 
        header('Refresh:0; url=index.php');
        }

} else {
    header('Refresh:0; url=index.php');
}
function imageResize($imageResourceId,$width,$height) {
    $targetWidth = $width < 1280 ? $width : 1280 ;
    $targetHeight = ($height/$width)* $targetWidth;
    $targetLayer = imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}


/** show details */
function size_as_kb($size = 0) {
    if($size < 1048576) {
        $size_kb = round($size / 1024, 2);
        return "{$size_kb} KB";
    } else {
        $size_mb = round($size / 1048576, 2);
        return "{$size_mb} MB";
    }
}

function imgSize($img) {
    $targetWidth = $img[0] < 1280 ? $img[0] : 1280 ;
    $targetHeight = ($img[1] / $img[0])* $targetWidth;
    return [round($targetWidth, 2), round($targetHeight, 2)];
}
?>