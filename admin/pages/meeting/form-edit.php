<?php 
    include_once('../authen.php'); 

    $id = $_GET['id'];

    $sql = "SELECT * FROM `report_title` WHERE `id_title` = '".$id."' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Meeting Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Meeting Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../meeting">Meeting Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update.php" method="post">
                        <div class="card-body">
                        <div class="form-group">
                                <label for="title_ban">ครั้ง</label>
                                <select name="report_time" class="form-control" required>
                                    <option value="" disabled selected>โปรดระบุครั้งที่ประชุม</option>
                                    <option value="1" <?php echo $row['report_time'] == '1' ? 'selected' : ' '; ?>>1</option>
                                    <option value="2" <?php echo $row['report_time'] == '2' ? 'selected' : ' '; ?>>2</option>
                                    <option value="3" <?php echo $row['report_time'] == '3' ? 'selected' : ' '; ?>>3</option>
                                    <option value="4" <?php echo $row['report_time'] == '4' ? 'selected' : ' '; ?>>4</option>
                                    <option value="5" <?php echo $row['report_time'] == '5' ? 'selected' : ' '; ?>>5</option>
                                    <option value="6" <?php echo $row['report_time'] == '6' ? 'selected' : ' '; ?>>6</option>
                                    <option value="7" <?php echo $row['report_time'] == '7' ? 'selected' : ' '; ?>>7</option>
                                    <option value="8" <?php echo $row['report_time'] == '8' ? 'selected' : ' '; ?>>8</option>
                                    <option value="9" <?php echo $row['report_time'] == '9' ? 'selected' : ' '; ?>>9</option>
                                    <option value="10" <?php echo $row['report_time'] == '10' ? 'selected' : ' '; ?>>10</option>
                                    <option value="11" <?php echo $row['report_time'] == '11' ? 'selected' : ' '; ?>>11</option>
                                    <option value="12" <?php echo $row['report_time'] == '12' ? 'selected' : ' '; ?>>12</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="link_ban">ปี</label>
                                <select name="year" class="form-control" required>
                                    <option value="" disabled selected>โปรดเลือกปี</option>
                                    <?php $year=date(Y)+541;
                                                $i=0;
                                                while ( $i <= 3 ){?>
                                    <option value="<?php echo $year;?>" <?php echo $row['year'] == $year ? 'selected' : ' '; ?>>ปี พ.ศ. <?php echo $year;?></option>
                                    <?php
                                    $year++;
                                    $i++;
                                    } ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>ในวันที่:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input name="report_date" type="date" class="form-control" value="<?php echo $row['report_date'];?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="location">สถานที่</label>
                                <input type="text" name="location" class="form-control" placeholder="ณ ......." value="<?php echo $row['location'];?>" required>
                            </div>
                            <div class="form-group">
                                <label>ประเภทวาระการประชุมคณะกรรม</label>
                                <select name="report_type" class="form-control" required>
                                    <option value="" disabled selected>โปรดระบุประเภทการประชุม</option>
                                    <option value="1" <?php echo $row['report_type'] == '1' ? 'selected' : ' '; ?>>ระเบียบวาระการประชุมคณะกรรมการวิชาการคณะ</option>
                                    <option value="2" <?php echo $row['report_type'] == '2' ? 'selected' : ' '; ?>>ระเบียบวาระการประชุมคณะกรรมการบริหารคณะ
                                    </option>
                                    <option value="3" <?php echo $row['report_type'] == '3' ? 'selected' : ' '; ?>>คณบดีสัญจร</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    

</body>

</html>