<?php 
    include_once('../authen.php'); 

    $id = $_GET['id'];

    $sql = "SELECT * FROM `report_detail` WHERE `id_detail` = '".$id."' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Detail Meeting Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Detail Meeting Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../meeting">Detail Meeting Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update-detail.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <input type="hidden" name="id" value="<?php echo $row['id_detail'];?>">
                            <div class="form-group">
                                <label for="title">หัวข้อ</label>
                                <select name="title" class="form-control" required>
                                    <option value="" disabled selected>โปรดเลือกหัวข้อ</option>
                                    <option value="1" <?php echo $row['title'] == '1' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๑ เรื่องประธานแจ้งให้ทราบ</option>
                                    <option value="2" <?php echo $row['title'] == '2' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๒ เรื่องรับรองรายงานการประชุม</option>
                                    <option value="3" <?php echo $row['title'] == '3' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๓ เรื่องสืบเนือง</option>
                                    <option value="4" <?php echo $row['title'] == '4' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๔ เรื่องเสนอเพื่อทราบ</option>
                                    <option value="5" <?php echo $row['title'] == '5' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๕ เรื่องเสนอเพื่อพิจารณา</option>
                                    <option value="6" <?php echo $row['title'] == '6' ? 'selected' : ' '; ?>>ระเบียบที่
                                        ๖ เรื่องอื่นๆ</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name_title">ชื่อหัวข้อ</label>
                                <input type="text" class="form-control" name="name_title" value="<?php echo $row['name_title'];?>" id="name_title" required
                                    placeholder="ชื่อหัวข้อ">
                            </div>

                            <div class="form-group">
                                <label for="customFile">เพิ่มไฟล์</label>
                                <input type="file" class="form-control-file" name="file" id="file" value="<?php echo $row['file'];?>" accept=".pdf"
                                    onchange="return fileValidation()">
                            </div>

                            <div class="form-group">
                                <label>หรือ</label>
                                <input type="text" name="link" class="form-control" value="<?php echo $row['link'];?>" placeholder="ลิงค์">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
    function fileValidation() {
        var fileInput = document.getElementById('file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.pdf)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .pdf เท่านั้น.');
            fileInput.value = '';
            return false;
        }
    }
    </script>

</body>

</html>