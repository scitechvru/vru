<?php include_once('../authen.php') ?>
<?php

if(isset($_POST['submit'])){
    $filesdoc = $_FILES['file']['tmp_name'];
    $filesdocname = 'Safety'.round(microtime(true)*9999);
    $path = "../../../assets/files/allfiles/";
    $arraytemp = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

    if($arraytemp=="pdf" or $arraytemp=="doc" or $arraytemp=="docx"){
        if(move_uploaded_file($filesdoc,$path.$filesdocname.'_edit.'. $arraytemp)){ 
            $sql = "UPDATE `tb_safetyuni` 
                    SET name_uni = '".$_POST['name_uni']."', 
                        link_uni = '".$_POST['link_uni']."', 
                        file_uni = '".$filesdocname.'_edit.'.$arraytemp."', 
                        type_uni = '".$_POST['type_uni']."', 
                        update_at = '".date('Y-m-d H:i:s')."'
                        WHERE id_files = '".$_POST['id']."' ";
            $result = $conn->query($sql) or die($conn->error);
            if($result){
                echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
                header('Refresh:0; url=index.php');
            }else{
                echo '<script> alert("Error Creating!")</script>'; 
                header('Refresh:0; url=index.php');
            }
        }
    } else {
        echo '<script> alert("ไฟล์ที่ท่านอัพโหลดไม่ใช่นามสกุลไล์ .pdf .docx .doc !")</script>'; 
        header('Refresh:0; url=index.php');
        }
} else {
    header('Refresh:0; url=index.php');
}
?>