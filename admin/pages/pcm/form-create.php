<?php 
include_once('../authen.php'); 
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Procurement Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Procurement Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../pcm">Procurement Management</a></li>
                                <li class="breadcrumb-item active">Create Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="create.php" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="titie_pcm">ชื่อไฟล์</label>
                                <input type="text" class="form-control" name="titie_pcm" id="titie_pcm" required
                                    placeholder="ชื่อไฟล์">
                            </div>
                            <div class="form-group">
                                <label>ประเภทจัดซื้อจัดจ้าง</label>
                                <select class="form-control" name="type_pcm" required>
                                    <option value="" disabled selected>โปรดระบุประเภทจัดซื้อจัดจ้าง</option>
                                    <option value="1">ประกาศจัดซื้อจัดจ้าง</option>
                                    <option value="2">ประกาศร่าง TOR</option>
                                    <option value="3">ประกาศผู้ชนะ</option>
                                    <option value="4">แผนการจัดซื้อจัดจ้าง</option>
                                    <option value="5">สรุปผลการจัดซื้อจัดจ้าง</option>
                                    <option value="6">วิเคราะผลการจัดซื้อจัดจ้าง</option>
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label>ปีงบประมาณ</label>
                                <select class="form-control" name="year" required>
                                    <option value="" disabled selected>โปรดระบุปีงบประมาณ</option>
                                    <?php 
                                   $curYear=date('Y')+543; // current Year
                                   $m=date('m'); // current month
                                   if($m>9) $curYear++; // ถ้าเดือนปัจจบันเป็น เดือน 10-12 ปีงบประมาณ = ปีหน้า =  ปีบัจจุบัน + 1
                                   $startYear=$curYear - 2; //กำหนดให้แสดงปีงบประมาณย้อนหลัง 3 ปี จากปีปัจจุบัน
                                   for($startYear; $startYear<=$curYear; $startYear++){
                                       ?>
                                    <option value="<?php echo "$startYear";?>"
                                    <?php $startYear==$curYear ? 'SELECTED' : ''?>>
                                    ปีพ.ศ.<?php echo "$startYear";?></option>
                                    <?php } // default current year?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>ไฟล์จัดซื้อจัดจ้าง</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="file" id="customFile"
                                        onchange="return fileValidation()" accept=".pdf, .docx, .doc" required>
                                </div>
                            </div>

                            <input type="checkbox" name="status" checked data-toggle="toggle" data-on="Active"
                                data-off="Block" data-onstyle="success" data-style="ios">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
    function fileValidation() {
        var fileInput = document.getElementById('customFile');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.pdf|\.doc|\.docx)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .pdf .docx .doc เท่านั้น.');
            fileInput.value = '';
            return false;
        }
    }
    </script>

</body>

</html>