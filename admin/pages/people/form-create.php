<?php 
include_once('../authen.php'); 
$sql = "SELECT int_major,nameth_major FROM tb_major ORDER BY id_major DESC";
$result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>People Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>People Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../people">People Management</a></li>
                                <li class="breadcrumb-item active">Create Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="create.php" method="post" enctype="multipart/form-data">
                        <div class="card-body">

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tname2_per">ตำแหน่งทางวิชาการ</label>
                                        <select name="tname2_per" class="form-control" required>
                                            <OPTION VALUE='' disabled selected> ----- Select ----- </OPTION>
                                            <OPTION VALUE=''>สายสนับสนุน</OPTION>
                                            <OPTION VALUE='อ.'>อาจารย์</OPTION>
                                            <OPTION VALUE='ผศ.'>ผู้ช่วยศาสตราจารย์</OPTION>
                                            <OPTION VALUE='รศ.'>รองศาสตราจารย์</OPTION>
                                            <OPTION VALUE='ศ.'>ศาสตราจารย์</OPTION>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="link_ban">ระดับการศึกษา</label>
                                        <select name="tname1_per" class="form-control">
                                            <OPTION VALUE='' disabled selected> ----- Select ----- </OPTION>
                                            <OPTION VALUE=''>ยังไม่จบปริญญาเอก</OPTION>
                                            <OPTION VALUE='ดร.'>ปริญญาเอก</OPTION>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tname_per">คำนำหน้าชื่อ</label>
                                        <select name="tname_per" class="form-control" required>
                                            <OPTION VALUE='' disabled selected> ----- Select ----- </OPTION>
                                            <OPTION VALUE='นางสาว'>นางสาว (Ms.)</OPTION>
                                            <OPTION VALUE='นาง'>นาง (Mrs.)</OPTION>
                                            <OPTION VALUE='นาย'>นาย (Mr.)</OPTION>
                                        </select>

                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="idcard_per">บัตรประชาชน</label>
                                        <input type="text" class="form-control" name="idcard_per" id="idcard_per"
                                            onkeyup="CardId(this)" required placeholder="บัตรประชาชน">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="fnamet_per">ชื่อไทย</label>
                                        <input type="text" class="form-control" name="fnamet_per" id="fnamet_per"
                                            required placeholder="ชื่อไทย">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="lnamet_per">นามสกุลไทย</label>
                                        <input type="text" class="form-control" name="lnamet_per" id="lnamet_per"
                                            required placeholder="นามสกุลไทย">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="fnamee_per">ชื่ออังกฤษ</label>
                                        <input type="text" class="form-control" name="fnamee_per" id="fnamee_per"
                                            required placeholder="ชื่ออังกฤษ">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="lnamee_per">นามสกุลอังกฤษ</label>
                                        <input type="text" class="form-control" name="lnamee_per" id="lnamee_per"
                                            required placeholder="นามสกุลอังกฤษ">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="bdate_per">วันเดือนปีเกิด</label>
                                        <input type="date" class="form-control" name="bdate_per" id="bdate_per"
                                            required>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>ประเภทบุคลากร</label>
                                        <select class="form-control" name="type_per" required>
                                            <option value="" disabled selected>โปรดระบุประเภทบุคลากร</option>
                                            <OPTION VALUE='1'>พนักงานมหาวิทยาลัยสายวิชาการ</OPTION>
                                            <OPTION VALUE='2'>พนักงานมหาวิทยาลัยสายสนับสนุน</OPTION>
                                            <OPTION VALUE='3'>ข้าราชการสายวิชาการ</OPTION>
                                            <OPTION VALUE='4'>ข้าราชการสายสนับสนุน</OPTION>
                                            <OPTION VALUE='5'>พนักงานประจำ</OPTION>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>สังกัดหลักสูตร</label>
                                        <select class="form-control" name="major_per" required>
                                            <option value="" disabled selected>โปรดระบุสังกัดของท่าน</option>
                                            <?php while ($row = $result->fetch_assoc()){?>
                                            <option value="<?php echo $row['int_major']?>">
                                                สังกัดหลักสูตร<?php echo $row['nameth_major']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="phone_per">เบอร์โทรศัพท์</label>
                                        <input type="text" class="form-control" name="phone_per" id="phone_per" maxlength="12"
                                            onKeyUp="TelMo(this)" required>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="email_per">อีเมลล์</label>
                                        <input type="email" class="form-control" name="email_per" id="email_per"
                                            required>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>รูปโปรไฟล์</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="file" id="customFile"
                                        onchange="return fileValidation()" accept=".jpg, .jpeg, .png" required>
                                </div>
                                <figure class="figure text-center d-none mt-2">
                                    <img id="imgUpload" class="figure-img img-fluid rounded" alt="">
                                </figure>
                            </div>


                            <input type="checkbox" name="status" checked data-toggle="toggle" data-on="Active"
                                data-off="Block" data-onstyle="success" data-style="ios">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
    //สคริปห้ามกรอกตัวหนังสือกรอกได้เฉพาะตัวเลข
    $("#phone_per").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function fileValidation() {
        var fileInput = document.getElementById('customFile');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .png .jpg .jpeg เท่านั้น.');
            fileInput.value = '';
            return false;
        }
    }
    $('.custom-file-input').on('change', function() {
        var size = this.files[0].size / 1024 / 1024
        //console.log(size)
        if (size.toFixed(2) > 2) {
            alert('รูปภาพของท่านใหญ่เกินไป')
        } else {
            var fileName = $(this).val().split('\\').pop()
            $(this).siblings('.custom-file-label').html(fileName)
            if (this.files[0]) {
                var reader = new FileReader()
                $('.figure').addClass('d-block')
                reader.onload = function(e) {
                    $('#imgUpload').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0])
            }
        }
    })

    function TelMo(obj) {
        var pattern = new String("___-___-____");
        var pattern_ex = new String("-");
        var returnText = new String("");
        var obj_l = obj.value.length;
        var obj_l2 = obj_l - 1;
        for (i = 0; i < pattern.length; i++) {
            if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                returnText += obj.value + pattern_ex;
                obj.value = returnText;
            }
        }
        if (obj_l >= pattern.length) {
            obj.value = obj.value.substr(0, pattern.length);
        }
    };

    function CardId(obj) {
        var pattern = new String("_-____-_____-_-__");
        var pattern_ex = new String("-");
        var returnText = new String("");
        var obj_l = obj.value.length;
        var obj_l2 = obj_l - 1;
        for (i = 0; i < pattern.length; i++) {
            if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                returnText += obj.value + pattern_ex;
                obj.value = returnText;
            }
        }
        if (obj_l >= pattern.length) {
            obj.value = obj.value.substr(0, pattern.length);
        }
    };
    </script>

</body>

</html>