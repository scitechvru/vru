<?php include_once('../authen.php') ?>
<?php 
// 1 แผนกลยุทธ์คณะวิทยาศาสตร์และเทคโนโลยี
// 2 แผนปฏิบัติการประจำปีงบประมาณ
// 3 รายงานตัวชี้วัดเป้าประสงค์ประจำปีงบประมาณ
// 4 รายงานตัวชี้วัดโครงการประจำปีงบประมาณ

?>
<?php

    if(isset($_POST['submit'])){

        $status = isset($_POST['status']) ? 'true' : 'false' ;
        $filesdoc = $_FILES['file']['tmp_name'];
        $filesdocname = 'files-plan'.round(microtime(true)*9999);
        $path = "../../../assets/files/plan/";
        $arraytemp = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        
        if($arraytemp=="pdf" or $arraytemp=="doc" or $arraytemp=="docx"){
            if(move_uploaded_file($filesdoc,$path.$filesdocname.'_origin.'. $arraytemp)){ 
            $sql = "INSERT INTO `tb_plan` (`titie_plan`, `file_plan`, `status_plan`, `year`, `type_plan`, `create_at`, `update_at`) 
            VALUES ('".$_POST['titie_plan']."', 
                    '".$filesdocname.'_origin.'.$arraytemp."',
                    '".$status."',
                    '".$_POST['year']."',
                    '".$_POST['type_plan']."',
                    '".date('Y-m-d H:i:s')."',
                    '".date('Y-m-d H:i:s')."')";
            $result = $conn->query($sql) or die($conn->error);
            if($result){
                    echo '<script> alert("Finished Creating!")</script>'; 
                    header('Refresh:0; url=index.php');
            }else{
                echo '<script> alert("Error Creating!")</script>'; 
                header('Refresh:0; url=index.php');
            }
        }
        }else {
            echo '<script> alert("ไฟล์ที่ท่านอัพโหลดไม่ใช่นามสกุลไล์ .pdf .docx .doc !")</script>'; 
            header('Refresh:0; url=index.php');
        }
 
    }else{
        header('Refresh:0; url=index.php');
    }
?>