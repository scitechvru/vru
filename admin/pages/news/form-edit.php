<?php 
  include_once('../authen.php');
  if(!isset($_GET['id'])){
    header('Location:index.php');
  }
  $sql = "SELECT * FROM `tb_news` WHERE `id_news` = '".$_GET['id']."' ";
  $result = $conn->query($sql);
  $row = $result->fetch_assoc();

  $sql1 = "SELECT int_major,nameth_major FROM tb_major ORDER BY id_major DESC";
  $result1 = $conn->query($sql1);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>News Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../../plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>News Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../news">News Management</a></li>
                                <li class="breadcrumb-item active">Update Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="update.php" method="post" enctype="multipart/form-data">
                        <div class="card-body">

                        <div class="form-group">
                                <label for="subject">หัวข้อข่าว</label>
                                <input type="text" class="form-control" id="subject" name="subject"
                                    placeholder="หัวข้อข่าว" value="<?php echo $row['title_news'];?>" required>
                            </div>

                            <div class="form-group">
                                <label>รูปปกข่าว</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="file" id="customFile" onchange="return fileValidation()" accept=".jpg, .jpeg, .png" required>
                                    
                                </div>
                                <figure class="figure text-center d-block mt-2">
                                    <input type="hidden" name="data_file" value="<?php echo $row['img_news']; ?>">
                                    <img id="imgUpload" src="../../../assets/images/news/<?php echo $row['img_news']; ?>"
                                        class="figure-img img-fluid rounded" alt="<?php echo $row['title_ban'];?>">
                                </figure>
                            </div>

                            <div class="form-group">
                                <label>ไฟล์ข่าว</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="file-news" id="file-news" onchange="return fileValidation2()" accept=".pdf" required>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="link-news">ลิงค์ข่าว</label>
                                <input type="text" class="form-control" id="link-news" name="link-news"
                                    placeholder="ลิงค์ข่าว" value="<?php echo $row['link_news'];?>" >
                            </div>

                            <div class="form-group">
                                <label for="date-news">วันที่</label>
                                <input type="date" class="form-control" id="date-news" name="date-news" 
                                     required>
                            </div>
                            <div class="form-group">
                                <label>โปรดระบุประเภทข่าว</label>
                                <select class="form-control" name="type_news" style="width: 100%;">
                                    <option value="" disabled selected>โปรดระบุประเภทข่าว</option>
                                    <option value="PR" <?php echo $row['type_news'] == 'PR' ? 'selected' : ' '; ?>>
                                        ข่าวประชาสัมพันธ์</option>
                                    <option value="STU" <?php echo $row['type_news'] == 'STU' ? 'selected' : ' '; ?>>
                                        ข่าวสำหรับนักศึกษา</option>
                                    <option value="ED" <?php echo $row['type_news'] == 'ED' ? 'selected' : ' '; ?>>
                                        ข่าวศึกษาต่อ</option>
                                    <option value="RE" <?php echo $row['type_news'] == 'RE' ? 'selected' : ' '; ?>>
                                        ข่าวรับสมัครงาน</option>
                                    <option value="AL" <?php echo $row['type_news'] == 'AL' ? 'selected' : ' '; ?>>
                                        ข่าวศิษย์เก่า</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>สังกัดหลักสูตร</label>
                                <select class="form-control" name="major" required>
                                    <option value="" disabled selected>โปรดระบุสังกัดของท่าน</option>
                                    <?php while ($row1 = $result1->fetch_assoc()){?>
                                    <option value="<?php echo $row1['int_major'];?>" <?php echo $row1['int_major'] == $row1['int_major'] ? 'selected' : ' '; ?>>ผู้ดูแล<?php echo $row1['nameth_major']?></option>
                                    <?php }?>
                                </select>
                            </div>

                            <input type="checkbox" name="status" <?php echo $row['status_news'] == 'true' ? 'checked': ''; ?>
                                data-toggle="toggle" data-on="Active" data-off="Block" data-onstyle="success"
                                data-style="ios">

                            <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">

                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


    <script>
    function fileValidation() {
        var fileInput = document.getElementById('customFile');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.png|\.jpg|\.jpeg)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .png .jpg .jpeg เท่านั้น.');
            fileInput.value = '';
            return false;
        }
    }

    function fileValidation2() {
        var fileInput = document.getElementById('file-news');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.pdf)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('รองรับแค่ไฟล์นามสกุล .pdf เท่านั้น.');
            fileInput.value = '';
            return false;
        }
    }
    $('.custom-file-input').on('change', function() {
        var size = this.files[0].size / 1024 / 1024
        if (size.toFixed(2) > 2) {
            alert('to big, maximum is 2MB')
        } else {
            var fileName = $(this).val().split('\\').pop()
            $(this).siblings('.custom-file-label').html(fileName)
            if (this.files[0]) {
                var reader = new FileReader()
                $('.figure').addClass('d-block')
                reader.onload = function(e) {
                    $('#imgUpload').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0])
            }
        }
    })
    </script>

</body>

</html>