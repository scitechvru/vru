<?php include_once('../authen.php') ?>
<?php
    if(isset($_POST['submit'])){
        $status = isset($_POST['status']) ? 'true' : 'false' ;
        $image_name = $_POST['data_file'];
        $filesdoc = $_FILES['file-news']['tmp_name'];
        $file = $_FILES['file']['tmp_name']; 
        $sourceProperties = getimagesize($file);
        $folderPath = "../../../assets/images/news/";
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $image_name = 'news-edit'.round(microtime(true)*9999);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($targetLayer,$folderPath. $fileNewName. ".". $ext);
                break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file); 
                    $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. $fileNewName. ".". $ext);
                    break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($targetLayer,$folderPath. $fileNewName. ".". $ext);
                break;

            default:
                echo "Invalid Image type.";
                exit;
                break;
            }


        $filesdocname = 'files-news-edit'.round(microtime(true)*9999);
        $path = "../../../assets/files/News/";
        $arraytemp = pathinfo($_FILES['file-news']['name'], PATHINFO_EXTENSION);
        
        if($arraytemp=="pdf"){
            move_uploaded_file($filesdoc, $path.$filesdocname.'.'. $arraytemp); 
        }else {
            echo '<script> alert("ไฟล์ที่ท่านอัพโหลดไม่ใช่นามสกุลไล์ pdf !")</script>'; 
            header('Refresh:0; url=index.php');
        }
        if(move_uploaded_file($file, $folderPath.$image_name.'.'. $ext) ){

            $sql = "UPDATE `tb_news` 
                    SET type_news = '".$_POST['subject']."', 
                        major_news = '".$_POST['major']."', 
                        title_news = '".$_POST['subject']."', 
                        name_news = '".$filesdocname.'.'.$arraytemp."', 
                        link_news = '".$_POST['link-news']."',  
                        img_news = '".$image_name.'.'. $ext."', 
                        date_news = '".$_POST['date-news']."', 
                        status = '".$status."', 
                        updated_at = '".date("Y-m-d H:i:s")."'
                        WHERE id_news = '".$_POST['id']."' ";
            $result = $conn->query($sql);
            if($result){
                echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
                header('Refresh:0; url=index.php');
            }else{
                echo '<script> alert("Error Creating!")</script>'; 
                header('Refresh:0; url=index.php');
            }
        }

    } else {
        header('Refresh:0; url=index.php');
    }
    
    function imageResize($imageResourceId,$width,$height) {
        $targetWidth = $width < 1280 ? $width : 1280 ;
        $targetHeight = ($height/$width)* $targetWidth;
        $targetLayer = imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }


    /** show details */
    function size_as_kb($size = 0) {
        if($size < 1048576) {
            $size_kb = round($size / 1024, 2);
            return "{$size_kb} KB";
        } else {
            $size_mb = round($size / 1048576, 2);
            return "{$size_mb} MB";
        }
    }

    function imgSize($img) {
        $targetWidth = $img[0] < 1280 ? $img[0] : 1280 ;
        $targetHeight = ($img[1] / $img[0])* $targetWidth;
        return [round($targetWidth, 2), round($targetHeight, 2)];
    }
?>