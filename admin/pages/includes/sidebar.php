<?php 
$uri = $_SERVER['REQUEST_URI'];
$array = explode('/',$uri);
$key = array_search("pages",$array);
$name = $array[$key+1];
?>
<nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-info">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                Last Login: <?php echo date_format(new DateTime($_SESSION['last_login']), "j F Y H:i:s" ) ?>
                <i class="fa fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light text-center d-block">Admin Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="../../../assets/image/logo.png" class="img-circle elevation-2" alt="ScitechVRU">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="../dashboard" class="nav-link <?php echo $name == 'dashboard' ? 'active': '' ?>">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <?php if($_SESSION['status'] == 'superadmin') {?>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link ">
                        <i class="fas fa-user-lock nav-icon"></i>
                        <p>
                            จัดการแอดมิน
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../admin" class="nav-link <?php echo $name == 'admin' ? 'active': '' ?>">
                                <i class="fas fa-users-cog nav-icon"></i>
                                <p>ผู้ดูแลระบบ</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../major" class="nav-link <?php echo $name == 'major' ? 'active': '' ?>">
                                <i class="fas fa-sitemap nav-icon"></i>
                                <p>สาขา</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../online" class="nav-link <?php echo $name == 'online' ? 'active': '' ?>">
                                <i class="fas fa-signal nav-icon"></i>
                                <p>บริการออนไลน์</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../userlog" class="nav-link <?php echo $name == 'userlogs' ? 'active': '' ?>">
                                <i class="fas fa-user-shield nav-icon"></i>
                                <p>Userlogs <span class="badge badge-light" id="notiScore"></span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../adminlog" class="nav-link <?php echo $name == 'adminlogs' ? 'active': '' ?>">
                                <i class="fas fa-user-shield nav-icon"></i>
                                <p>Adminlogs <span class="badge badge-light" id="notiScore"></span></p>
                            </a>
                        </li>
                    </ul>
                </li>

                <?php }?>
                <?php if($_SESSION['status'] == 'superadmin' || $_SESSION['status'] == 'hradmin')  {?>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link ">
                        <i class="fas fa-user-lock nav-icon"></i>
                        <p>
                            จัดการฝ่ายบุคคล
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../people" class="nav-link <?php echo $name == 'people' ? 'active': '' ?>">
                                <i class="fas fa-users nav-icon"></i>
                                <p>อาจารย์และบุคลากร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../pm" class="nav-link <?php echo $name == 'pm' ? 'active': '' ?>">
                                <i class="fas fa-chalkboard-teacher nav-icon"></i>
                                <p>ระบบประเมิน</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php }?>
                <?php if($_SESSION['status'] == 'superadmin' || $_SESSION['major_admin'] == 'OF') {?>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link ">
                        <i class="fas fa-user-lock nav-icon"></i>
                        <p>
                            จัดการภายในคณะ
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="../publicize" class="nav-link <?php echo $name == 'publicize' ? 'active': '' ?>">
                                <i class="fas fa-image nav-icon"></i>
                                <p>แบนเนอร์ประชาสัมพันธ์</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../pcm" class="nav-link <?php echo $name == 'plan' ? 'active': '' ?>">
                                <i class="fas fa-money-bill nav-icon"></i>
                                <p>จัดซื้อจัดจ้าง</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../plan" class="nav-link <?php echo $name == 'plan' ? 'active': '' ?>">
                                <i class="fas fa-chalkboard-teacher nav-icon"></i>
                                <p>แผนยุทธศาสตร์</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../allfiles" class="nav-link <?php echo $name == 'allfiles' ? 'active': '' ?>">
                                <i class="fas fa-file nav-icon"></i>
                                <p>ไฟล์ดาวน์โหลด</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../meeting" class="nav-link <?php echo $name == 'meeting' ? 'active': '' ?>">
                                <i class="fas fa-handshake nav-icon"></i>
                                <p>วาระการประชุม</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../ita" class="nav-link <?php echo $name == 'ita' ? 'active': '' ?>">
                                <i class="fas fa-tags nav-icon"></i>
                                <p>ITA</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../greenoffice"
                                class="nav-link <?php echo $name == 'greenoffice' ? 'active': '' ?>">
                                <i class="fas fa-tree nav-icon"></i>
                                <p>GreenOffice</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../safety" class="nav-link <?php echo $name == 'safety' ? 'active': '' ?>">
                                <i class="fas fa-university nav-icon"></i>
                                <p>สถานศึกษาปลอดภัย</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../shortcoures"
                                class="nav-link <?php echo $name == 'shortcoures' ? 'active': '' ?>">
                                <i class="fas fa-folder nav-icon"></i>
                                <p>หลักสูตรระยะสั้น</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../articles" class="nav-link <?php echo $name == 'articles' ? 'active': '' ?>">
                                <i class="fas fa-rss nav-icon"></i>
                                <p>วิจัยและวรสาร</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../video" class="nav-link <?php echo $name == 'video' ? 'active': '' ?>">
                                <i class="fas fa-video nav-icon"></i>
                                <p>วิดีโอเผยแพร่</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="../contacts" class="nav-link <?php echo $name == 'contacts' ? 'active': '' ?>">
                                <i class="fas fa-comment-alt nav-icon"></i>
                                <p>ผู้ติดต่อ</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php }?>
                <li class="nav-item">
                    <a href="../banner" class="nav-link <?php echo $name == 'banner' ? 'active': '' ?>">
                        <i class="fas fa-image nav-icon"></i>
                        <p>แบนเนอร์</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="../news" class="nav-link <?php echo $name == 'news' ? 'active': '' ?>">
                        <i class="fas fa-newspaper nav-icon"></i>
                        <p>ข่าว</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="../activity" class="nav-link <?php echo $name == 'activity' ? 'active': '' ?>">
                        <i class="fas fa-share-alt-square nav-icon"></i>
                        <p>กิจกรรม</p>
                    </a>
                </li>


                <li class="nav-header">Account Settings</li>
                <li class="nav-item">
                    <a href="../../logout.php" class="nav-link">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>