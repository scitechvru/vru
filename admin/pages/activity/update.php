<?php include_once('../authen.php') ?>
<?php
if(isset($_POST['submit'])){
    function uppic_only($img, $index, $imglocate, $limit_size = 9000000000, $limit_width = 0, $limit_height = 0){
        $allowed_types = array("JPG", "JPEG", "jpg", "jpeg", "png");
        if ($img["name"][$index] != "") {
            $fileupload1 = $img["tmp_name"][$index];
            $data_Img = @getimagesize($fileupload1);
            $g_img = explode(".", $img["name"][$index]);
            $ext1 = strtolower(array_pop($g_img));
            $file_up = time() . "_" . $index . "." . $ext1;
            $canUpload = 0;
            if (isset($data_Img) && $data_Img[0] > 0 && $data_Img[1] > 0) {
                if ($img["size"][$index] <= $limit_size) {
                    if ($limit_width > 0 && $limit_height > 0) {
                        if ($data_Img[0] <= $limit_width && $data_Img[1] <= $limit_height) {
                            $canUpload = 1;
                        }
                    } elseif ($limit_width > 0 && $limit_height == 0) {
                        if ($data_Img[0] <= $limit_width) {
                            $canUpload = 1;
                        }
                    } elseif ($limit_width == 0 && $limit_height > 0) {
                        if ($data_Img[1] <= $limit_height) {
                            $canUpload = 1;
                        }
                    } else {
                        $canUpload = 1;
                    }
                } else {
    
                }
            }
            if ($fileupload1 != "" && @in_array($ext1, $allowed_types) && $canUpload == 1) {
                @copy($fileupload1, $imglocate . $file_up);
                //    @chmod($imglocate.$file_up,0777);
            } else {
                $file_up = "";
            }
        }
        return $file_up; // ส่งกลับชื่อไฟล์
        //return $Title_image;
    }
    $fileNewName = $_POST['data_file'];

    $status = isset($_POST['status']) ? 'true' : 'false' ;
    $detail = str_replace('../../../','./',$_POST['detail']);
    $file = $_FILES['picco_image']['tmp_name']; 
    $sourceProperties = getimagesize($file);
    $folderPath = "../../../assets/images/activity/";
    $ext = pathinfo($_FILES['picco_image']['name'], PATHINFO_EXTENSION);
    $fileNewName = 'activity_cover'.round(microtime(true)*9999);
    $imageType = $sourceProperties[2];
    $num_pic = count($_FILES['file_upload']['name']);

        switch ($imageType) {

        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagepng($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        case IMAGETYPE_GIF:
            $imageResourceId = imagecreatefromgif($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagegif($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagejpeg($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        default:
            echo "Invalid Image type.";
            exit;
            break;
                }
        $cover = move_uploaded_file($file, $folderPath); 
        if ($num_pic > 0 && $_FILES['file_upload']['name'][0] != "") { // ถ้ามีอย่างน้อยหนึ่งรูป
            for ($i = 0; $i < $num_pic; $i++) { // วนลูปอัพรูป
                $upfile_name = uppic_only($_FILES['file_upload'], $i, "../../../assets/images/activity/"); 
                $sql1 = "SELECT cover_img FROM `tb_picup` WHERE id_img = '$id'";
                $result1 = $conn->query($sql1);
                while($row1 = $result1->fetch_assoc()){
                            $sql = "UPDATE `tb_picup` 
                            SET `title_img` = '".$_POST['title_img']."', 
                                `detail_img` = '".$detail."', 
                                `major_img` = '".$_POST['major']."',  
                                `date_img` = '".$_POST['date_img']."', 
                                `cover_img` = '".$fileNewName.'.'.$ext."', 
                                `place_img` = '".$_POST['place_img']."',  
                                `file_img` = '".$upfile_name."',  
                                `img_type` = '".$_POST['img_type']."',  
                                `update_at` = '".date("Y-m-d H:i:s")."'
                                WHERE `cover_img` = '".$_POST['cover_img']."'";
                    $result = $conn->query($sql) or die($conn->error);    
                }
                if($result){
                    echo '<script> alert("Finished Creating!")</script>'; 
                    header('Refresh:0; url=index.php');
                }else{
                    echo '<script> alert("Error Creating!")</script>'; 
                    header('Refresh:0; url=index.php');
                }
            }
        }else{
        header('Refresh:0; url=index.php');
        }
    }


        function imageResize($imageResourceId,$width,$height) {
        $targetWidth = $width < 1280 ? $width : 1280 ;
        $targetHeight = ($height/$width)* $targetWidth;
        $targetLayer = imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
        }


        /** show details */
        function size_as_kb($size = 0) {
        if($size < 1048576) {
        $size_kb = round($size / 1024, 2);
        return "{$size_kb} KB";
        } else {
        $size_mb = round($size / 1048576, 2);
        return "{$size_mb} MB";
        }
        }

        function imgSize($img) {
        $targetWidth = $img[0] < 1280 ? $img[0] : 1280 ;
        $targetHeight = ($img[1] / $img[0])* $targetWidth;
        return [round($targetWidth, 2), round($targetHeight, 2)];
        }

?>