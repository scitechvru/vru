<?php 
    include_once('../authen.php'); 

    if(!isset($_GET['id'])){
        header('Location:index.php');
    }
    $id = $_GET['id'];

    $sql = "SELECT * FROM `tb_picup` WHERE `id_img` = '".$id."' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    

    $sql1 = "SELECT int_major,nameth_major FROM tb_major ORDER BY id_major DESC";
    $result1 = $conn->query($sql1);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Activity Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Activity Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../activity">Activity Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update.php" method="post">
                    <div class="card-body">
                            <div class="form-group">
                                <label for="title_img">ชื่อกิจกรรม</label>
                                <input type="text" class="form-control" name="title_img" id="title_img" value="<?php echo $row['title_img']; ?>"required
                                    placeholder="ชื่อกิจกรรม">
                            </div>
                            <div class="form-group">
                                <label for="date-img">วันที่</label>
                                <input type="date"  class="form-control" id="date-img" name="date_img"
                                     required>
                            </div>
                            <div class="form-group">
                                <label for="place_img">สถานที่จัดกิจกรรม</label>
                                <input type="text" class="form-control" name="place_img" id="place_img" value="<?php echo $row['place_img']; ?>" required
                                    placeholder="สถานที่จัดกิจกรรม">
                            </div>
                            <div class="form-group">
                                <label>ประเภทกิจกรรม</label>
                                <select class="form-control" name="img_type" required>
                                    <option value="" disabled selected>โปรดระบุประเภทกิจกรรม</option>
                                   <option value="0"<?php echo $row['img_type'] == '0' ? 'selected' : ' '; ?>>กิจกรรมทั่วไป</option>
                                   <option value="1"<?php echo $row['img_type'] == '1' ? 'selected' : ' '; ?>>กิจกรรมด้านศิลปวัฒนธรรม</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>สังกัดหลักสูตร</label>
                                <select class="form-control" name="major" required>
                                    <option value="" disabled selected>โปรดระบุสังกัดของท่าน</option>
                                    <?php while ($row1 = $result1->fetch_assoc()){?>
                                    <option value="<?php echo $row1['int_major'];?>"
                                        <?php echo $row1['int_major'] == $row1['int_major'] ? 'selected' : ' ' ?>>
                                        ผู้ดูแล<?php echo $row1['nameth_major']?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        รายละเอียดกิจกรรม
                                    </h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool btn-sm" data-widget="collapse"
                                            data-toggle="tooltip" title="Collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="mb-3">
                                        <textarea class="d-none" name="detail" id="detail" rows="10" cols="80">
                                        <?php echo  str_replace('./', '../../../', $row['detail_img'] ); ?>
                                    </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>รูปปกกิจกรรม</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="picco_image" id="customFile" onchange="return fileValidation()" accept=".jpg, .jpeg, .png" required>
                                    
                                </div>
                                <figure class="figure text-center d-block mt-2">
                                    <input type="hidden" name="data_file" value="<?php echo $row['cover_img']; ?>">
                                    <img id="imgUpload" src="../../../assets/images/activity/<?php echo $row['cover_img']; ?>"
                                        class="figure-img img-fluid rounded" alt="<?php echo $row['title_img'];?>">
                                </figure>
                            </div>
                            <div class="form-group">
                                <label>รูปกิจกรรม</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" id="upload" onchange="return fileValidation()" accept=".jpg, .jpeg, .png">
                                    
                                </div>
                                <figure class="figure text-center d-block mt-2">
                                    <!-- Image preview -->
                                    <div id="thumbnail" class="figure-img img-fluid rounded" alt="" ></div>
                                </figure>
                            </div>
                        
                            
                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
           $(function() {
    $("#upload").on("click", function(e) {

        var lastFile = $(".file_upload:last").length;
        if (lastFile >= 0) {
            if (lastFile == 0 || $(".file_upload:last").val() != "") {
                var objFile = $("<input>", {
                    "class": "file_upload",
                    "type": "file",
                    "multiple": "true",
                    "name": "file_upload[]",
                    "style": "display:none",
                    change: function(e) {
                        var files = this.files
                        showThumbnail(files)
                    }
                });
                $(this).before(objFile);
                $(".file_upload:last").show().click().hide();
            } else {
                $(".file_upload:last").show().click().hide();
            }
        }
        e.preventDefault();
    });

    function showThumbnail(files) {

        //    $("#thumbnail").html("");
        for (var i = 0; i < files.length; i++) {
            var file = files[i]
            var imageType = /image.*/
            if (!file.type.match(imageType)) {
                console.log("Not an Image");
                continue;
            }

            var image = document.createElement("img");
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            thumbnail.appendChild(image);

            var reader = new FileReader();
            reader.onload = (function(aImg) {
                return function(e) {
                    aImg.src = e.target.result;
                };
            }(image))

            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload = function() {
                ctx.drawImage(image, 100, 100)
            }
        } // end for loop

    } // end showThumbnail

});
        function fileValidation() {
            var fileInput = document.getElementById('customFile');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.png|\.jpg|\.jpeg)$/i;
            if (!allowedExtensions.exec(filePath)) {
                alert('รองรับแค่ไฟล์นามสกุล .png .jpg .jpeg เท่านั้น.');
                fileInput.value = '';
                return false;
            }
        }
    $('.custom-file-input').on('change', function() {
        var size = this.files[0].size / 1024 / 1024
        //console.log(size)
        if (size.toFixed(2) > 2) {
            alert('รูปภาพของท่านใหญ่เกินไป')
        } else {
            var fileName = $(this).val().split('\\').pop()
            $(this).siblings('.custom-file-label').html(fileName)
            if (this.files[0]) {
                var reader = new FileReader()
                $('.figure').addClass('d-block')
                reader.onload = function(e) {
                    $('#imgUpload').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0])
            }
        }
    })
    //CKEDITOR
    CKEDITOR.replace('detail', {
        filebrowserBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
    });
    </script>

</body>

</html>