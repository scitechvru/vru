<?php include_once('../authen.php') ?>
<?php

if(isset($_POST['submit'])){
    $fileNewName = $_POST['data_file'];
    if($_FILES['file']['name'] != ''){
        $file = $_FILES['file']['tmp_name']; 
        $sourceProperties = getimagesize($file);
        $folderPath = "../../../assets/images/banner/";
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $fileNewName = 'banner_edit'.round(microtime(true)*9999);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($targetLayer,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($targetLayer,$folderPath. $fileNewName. ".". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($targetLayer,$folderPath. $fileNewName. ".". $ext);
                break;

            default:
                echo "Invalid Image type.";
                exit;
                break;
        }

        if ( move_uploaded_file($file, $folderPath.$fileNewName.'.'. $ext) ){
            $image_delete = ROOT_PATH . $folderPath . pathinfo($_POST['data_file'], PATHINFO_BASENAME);
            unlink($image_delete);
        }else{
            echo '<script> alert("ไม่สามารถอัพโหลดรูปภาพใหม่ได้ โปรดลองอีกครั้ง")</script>'; 
            header('Refresh:0; url=index.php'); 
        }
    }

    $detail = str_replace('../../../', './', $_POST['detail'] );

    $sql = "UPDATE `tb_banner` 
            SET `name_ban` = '"..$fileNewName.'.'. $ext."', 
                `major_ban` = '".$_POST['major']."', 
                `title_ban` = '".$_POST['title_ban']."',  
                `detail_ban` = '".$detail."', 
                `link_ban` = '".$_POST['link_ban']."',  
                `update_at` = '".date("Y-m-d H:i:s")."'
                WHERE id_ban = '".$_POST['id']."'; ";
    $result = $conn->query($sql);
    if($result){
        echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
        header('Refresh:0; url=index.php');
    }

} else {
    header('Refresh:0; url=index.php');
}
?>