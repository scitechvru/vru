<?php 
    include_once('../authen.php'); 
    if(!isset($_GET['id'])){
        header('Location:index.php');
    }
    $id = $_GET['id'];
    $sql = "SELECT first_name,last_name,username,status FROM `admin` WHERE `id` = '".$id."' ";
    $result = $conn->query($sql) or die($conn->error);
    $row = $result->fetch_assoc();
    $sql1 = "SELECT * FROM tb_major ORDER BY id_major DESC";
    $result1 = $conn->query($sql1);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- Favicons -->
 <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Admin Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../admin">Admin Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update.php" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" disabled class="form-control" name="username" id="username" required
                                    placeholder="Username" value="<?php echo $row['username'];?>">
                            </div>
                            <div class="form-group">
                                <label for="firstName">ชื่อ</label>
                                <input type="text" class="form-control" id="firstName" name="first_name"required
                                    placeholder="FirstName" value="<?php echo $row['first_name'];?>">
                            </div>
                            <div class="form-group">
                                <label for="lastName">นามสกุล</label>
                                <input type="text" class="form-control" id="lastName" name="last_name" required
                                    placeholder="LastName" value="<?php echo $row['last_name'];?>">
                            </div>
                            <div class="form-group">
                                <label>ตำแหน่ง</label>
                                <select class="form-control" name="status" required>
                                    <option value="" disabled selected>โปรดระบุตำแหน่ง</option>
                                    <option value="superadmin" <?php echo $row['status'] == 'superadmin' ? 'selected' : ' '; ?>>SuperAdmin</option>
                                    <option value="hradmin" <?php echo $row['status'] == 'hradmin' ? 'selected' : ' '; ?>>HRAdmin</option>
                                    <option value="admin"<?php echo $row['status'] == 'admin' ? 'selected' : ' '; ?>>Admin</option>
                                    <option value="leader"<?php echo $row['status'] == 'leader' ? 'selected' : ' '; ?>>LeaderAdmin</option>
                                    <option value="dean"<?php echo $row['status'] == 'dean' ? 'selected' : ' '; ?>>Dean</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>สังกัดหลักสูตร</label>
                                <select class="form-control" name="major" required>
                                    <option value="" disabled selected>โปรดระบุสังกัดของท่าน</option>
                                    <?php while ($row1 = $result1->fetch_assoc()){?>
                                    <option value="<?php echo $row1['int_major'];?>" <?php echo $row1['int_major'] == $row1['int_major'] ? 'selected' : ' '; ?>>ผู้ดูแล<?php echo $row1['nameth_major']?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
    $(function() {
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
    </script>

</body>

</html>