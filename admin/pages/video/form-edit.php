<?php 
    include_once('../authen.php'); 

    $id = $_GET['id'];

    $sql = "SELECT * FROM `tb_youtube` WHERE `id_youtube` = '".$id."' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    $sql1 = "SELECT int_major,nameth_major FROM tb_major ORDER BY id_major DESC";
    $result1 = $conn->query($sql1);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Video Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Video Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../video">Video Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update.php" method="post">
                        <div class="card-body">
                        <div class="form-group">
                                <label for="title_youtube">ชื่อวิดีโอ</label>
                                <input type="text" class="form-control" name="title_youtube" id="title_youtube" required
                                    placeholder="ชื่อวิดีโอ" value="<?php echo $row['title_youtube'];?>">
                            </div>
                            <div class="form-group">
                                <label for="link_youtube">ลิงค์เชื่อมโยงวิดีโอ</label>
                                <input type="text" class="form-control" name="link_youtube" id="link_youtube" required
                                    placeholder="ลิงค์เชื่อมโยงวิดีโอ"value="<?php echo $row['link_youtube'];?>">
                            </div>
                            <div class="form-group">
                                <label for="youtube_code">Code วิดีโอเผยแพร่ Youtube</label>
                                <input type="text" class="form-control" name="youtube_code" id="youtube_code" required
                                    placeholder="ตัวอย่าง https://youtu.be/code_video" value="<?php echo $row['youtube_code'];?>">
                            </div>
                            <div class="form-group">
                                <label for="date_youtube">วันที่</label>
                                <input type="date" class="form-control" name="date_youtube" id="date_youtube" value="<?php echo $row['date_youtube']; ?>">required>
                            </div>
                            <div class="form-group">
                                <label>สังกัดหลักสูตร</label>
                                <select class="form-control" name="major" required>
                                    <option value="" disabled selected>โปรดระบุสังกัดของท่าน</option>
                                    <?php while ($row1 = $result1->fetch_assoc()){?>
                                    <option value="superadmin"
                                        <?php echo $row1['int_major'] == $row1['int_major'] ? 'selected' : ' ' ?>>
                                        ผู้ดูแล<?php echo $row1['nameth_major']?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $row['id_youtube'];?>">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    
</body>

</html>