<?php 
    include_once('../authen.php'); 

    $id = $_GET['id'];

    $sql = "SELECT * FROM `tb_files` WHERE `id_files` = '".$id."' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Files Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Files Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="../allfiles">Files Management</a></li>
                                <li class="breadcrumb-item active">Edit Data</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="update.php" method="post">
                        <div class="card-body">
                        <div class="form-group">
                                <label for="title_files">ชื่อไฟล์ดาวน์โหลด</label>
                                <input type="text" class="form-control" name="title_files" id="title_files" required
                                    placeholder="ชื่อไฟล์ดาวน์โหลด" value="<?php echo $row['title_files'];?>">
                            </div>
                            <div class="form-group">
                                <label for="link_files">ลิงค์เชื่อมโยงไฟล์ดาวน์โหลด</label>
                                <input type="text" class="form-control" name="link_files" id="link_files" required
                                    placeholder="ลิงค์เชื่อมโยงไฟล์ดาวน์โหลด" value="<?php echo $row['link_files'];?>">
                            </div>
                            <div class="form-group">
                                <label>ประเภทไฟล์ดาวน์โหลด</label>
                                <select class="form-control" name="type_files" required>
                                    <option value="" disabled selected>โปรดระบุประเภทไฟล์ดาวน์โหลด</option>
                                    <option value="stu" <?php echo $row['type_files'] == 'stu' ? 'selected' : ' '; ?>> แบบฟอร์มนักศึกษา</option>
                                    <option value="man" <?php echo $row['type_files'] == 'man' ? 'selected' : ' '; ?>> คู่มือนักศึกษา</option>
                                    <option value="per" <?php echo $row['type_files'] == 'per' ? 'selected' : ' '; ?>>แบบฟอร์มอาจารย์/เจ้าหน้าที่</option>
                                    <option value="res" <?php echo $row['type_files'] == 'res' ? 'selected' : ' '; ?>> แบบฟอร์มวิจัย</option>
                                    <option value="info" <?php echo $row['type_files'] == 'info' ? 'selected' : ' '; ?>> สารสนเทศ</option>
                                    <option value="reg" <?php echo $row['type_files'] == 'reg' ? 'selected' : ' '; ?>>ระเบียบ/ข้อบังคับ/กฎหมายที่เกี่ยวข้อง</option>
                                    <option value="qua" <?php echo $row['type_files'] == 'qua' ? 'selected' : ' '; ?>>ประกันคุณภาพ</option>
                                    <option value="mua" <?php echo $row['type_files'] == 'mua' ? 'selected' : ' '; ?>>คู่มือ</option>
                                    <option value="jr1" <?php echo $row['type_files'] == 'jr1' ? 'selected' : ' '; ?>>ระเบียบการตีพิมพ์ JRIST</option>
                                    <option value="jr2" <?php echo $row['type_files'] == 'jr2' ? 'selected' : ' '; ?>>รูปแบบบทความวิชาการ JRIST</option>
                                    <option value="jr3" <?php echo $row['type_files'] == 'jr3' ? 'selected' : ' '; ?>>รูปแบบบทความวิจัย JRIST</option>
                                    <option value="jr4" <?php echo $row['type_files'] == 'jr4' ? 'selected' : ' '; ?>>ใบนำส่งบทความฯ JRIST</option>
                                    
                                </select>
                            </div>
                           
                            <div class="form-group">
                                <label>ไฟล์ดาวน์โหลด</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control-file" name="file" id="customFile"
                                        onchange="return fileValidation()" accept=".pdf, .docx, .doc" required>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $row['id_files'];?>">
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <!-- CK Editor -->
    <script src="../../plugins/ckeditor/ckeditor.js"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
    $(function() {
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
    $('.custom-file-input').on('change', function() {
        var size = this.files[0].size / 1024 / 1024
        //console.log(size)
        if (size.toFixed(2) > 2) {
            alert('รูปภาพของท่านใหญ่เกินไป')
        } else {
            var fileName = $(this).val().split('\\').pop()
            $(this).siblings('.custom-file-label').html(fileName)
            if (this.files[0]) {
                var reader = new FileReader()
                $('.figure').addClass('d-block')
                reader.onload = function(e) {
                    $('#imgUpload').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0])
            }
        }
    })
    //CKEDITOR
    CKEDITOR.replace('detail', {
        filebrowserBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '../../plugins/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
    });
    </script>

</body>

</html>