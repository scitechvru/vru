<?php include_once('../authen.php') ?>
<?php

if(isset($_POST['submit'])){
    $filesdoc = $_FILES['file']['tmp_name'];
    $filesdocname = 'files'.round(microtime(true)*9999);
    $path = "../../../assets/files/allfiles/";
    $arraytemp = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

    if($arraytemp=="pdf" or $arraytemp=="doc" or $arraytemp=="docx"){
        if(move_uploaded_file($filesdoc,$path.$filesdocname.'_edit.'. $arraytemp)){ 
            $sql = "UPDATE `tb_files` 
                    SET name_files = '".$filesdocname.'_edit.'. $arraytemp."', 
                        link_files = '".$_POST['link_files']."', 
                        title_files = '".$_POST['title_files']."', 
                        type_files = '".$_POST['type_files']."'
                        WHERE id_files = '".$_POST['id']."' ";
            $result = $conn->query($sql) or die($conn->error);
            if($result){
                echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
                header('Refresh:0; url=index.php');
            }else{
                echo '<script> alert("Error Creating!")</script>'; 
                header('Refresh:0; url=index.php');
            }
        }
    } else {
        echo '<script> alert("ไฟล์ที่ท่านอัพโหลดไม่ใช่นามสกุลไล์ .pdf .docx .doc !")</script>'; 
        header('Refresh:0; url=index.php');
        }
} else {
    header('Refresh:0; url=index.php');
}
?>