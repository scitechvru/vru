<?php include_once('../authen.php') ?>
<?php
if(isset($_POST['submit'])){
       
    $status = isset($_POST['status']) ? 'true' : 'false' ;
    $detail = str_replace('../../../','./',$_POST['detail']);
    $file = $_FILES['file']['tmp_name']; 
    $sourceProperties = getimagesize($file);
    $folderPath = "../../../assets/images/articles/";
    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
    $fileNewName = 'articles'.round(microtime(true)*9999);
    $imageType = $sourceProperties[2];

    switch ($imageType) {

        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagepng($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        case IMAGETYPE_GIF:
            $imageResourceId = imagecreatefromgif($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagegif($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($file); 
            $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
            imagejpeg($targetLayer,$folderPath. $fileNewName. ".". $ext);
            break;

        default:
            echo "Invalid Image type.";
            exit;
            break;
    }
    
    if(move_uploaded_file($file, $folderPath.$fileNewName.'.'. $ext)){
        $sql = "UPDATE `articles` 
            SET `subject` = '".$_POST['subject']."', 
                `link` = '".$_POST['link']."', 
                `detail` = '".$detail."',  
                `image` = '".$fileNewName.'.'.$ext."', 
                `updated_at` = '".date("Y-m-d H:i:s")."'
                WHERE id = '".$_POST['id']."' ";
        $result = $conn->query($sql) or die($conn->error);
        if($result){
                echo '<script> alert("Finished Creating!")</script>'; 
                header('Refresh:0; url=index.php');
        }else{
            echo '<script> alert("Error Creating!")</script>'; 
            header('Refresh:0; url=index.php');
        }
    }
}else{
    header('Refresh:0; url=index.php');
}
function imageResize($imageResourceId,$width,$height) {
    $targetWidth = $width < 1280 ? $width : 1280 ;
    $targetHeight = ($height/$width)* $targetWidth;
    $targetLayer = imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
    return $targetLayer;
}


/** show details */
function size_as_kb($size = 0) {
    if($size < 1048576) {
        $size_kb = round($size / 1024, 2);
        return "{$size_kb} KB";
    } else {
        $size_mb = round($size / 1048576, 2);
        return "{$size_mb} MB";
    }
}

function imgSize($img) {
    $targetWidth = $img[0] < 1280 ? $img[0] : 1280 ;
    $targetHeight = ($img[1] / $img[0])* $targetWidth;
    return [round($targetWidth, 2), round($targetHeight, 2)];
}
?>