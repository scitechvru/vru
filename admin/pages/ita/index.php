<?php 
      include_once('../authen.php');

      $sql = "SELECT * FROM `report_title` ORDER BY id_title DESC";
      $result = $conn->query($sql);
      
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Meeting Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../../assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../../assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="../../../assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="../../../assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="../../../assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- Custom style -->
    <link rel="stylesheet" href="../../dist/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- bootstrap-toggle -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar & Main Sidebar Container -->
        <?php include_once('../includes/sidebar.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Meeting Management</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item active">Meeting Management</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title d-inline-block">Meeting List</h3>
                        <a href="form-create.php" class="btn btn-primary float-right ">Add Meeting +</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>ประเภทการประชุม</th>
                                    <th>ครั้ง/ปี</th>
                                    <th>รายละเอียด</th>
                                    <th>จัดการ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                  $num = 0;
                                  while ($row = $result->fetch_assoc()){
                                  $num++;
                                ?>
                                <tr>
                                    <td><?php echo $num;?></td>
                                    <td><?php switch($row['report_type']){
                                      case 1: echo "วารการประชุมกรรมการบริหารคณะ"; 
                                            break;
                                      case 2: echo "วารการประชุมกรรมการวิชาการคณะ"; 
                                            break;
                                      case 3: echo "คณบดีสัญจร"; 
                                            break;
                                    };?></td>
                                    <td><span
                                            class="badge badge-primary"><?php echo $row['report_time'].'/'.$row['year']?></span>
                                    </td>
                                    <td>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=1" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 1
                                        </a>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=2" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 2
                                        </a>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=3" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 3
                                        </a>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=4" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 4
                                        </a>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=5" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 5
                                        </a>
                                        <a href="view-detail.php?id=<?php echo $row['id_title']; ?>&title=6" class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-eye"></i> หัวข้อที่ 6
                                        </a>
                                    </td>
                                    <td>

                                        <a href="form-create-detail.php?id=<?php echo $row['id_title']; ?>"
                                            class="btn btn-sm btn-primary text-white">Add Details Meeting +</a>
                                        <a href="form-edit.php?id=<?php echo $row['id_title']; ?>"
                                            class="btn btn-sm btn-warning text-white">
                                            <i class="fas fa-edit"></i> edit
                                        </a>
                                        <a href="#" onclick="deleteItem(<?php echo $row['id_title']; ?>);"
                                            class="btn btn-sm btn-danger">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>

                                    </td>
                                </tr>

                                

                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->




        <!-- footer -->
        <?php include_once('../includes/footer.php') ?>
        <?php include_once('../../../php/adminlogs.php') ?>

    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SlimScroll -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js">
    </script>
    <script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- bootstrap-toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
        $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
    function deleteItem(id) {
        if (confirm('Are you sure, you want to delete this item?') == true) {
            window.location = `delete.php?id=${id}`;
            // window.location='delete.php?id='+id;
        }
    };

    function deleteDetail(id) {
        if (confirm('Are you sure, you want to delete this item?') == true) {
            window.location = `delete-detail.php?id=${id}`;
            // window.location='delete.php?id='+id;
        }
    };
    </script>

</body>

</html>