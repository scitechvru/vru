<?php include_once('../authen.php') ?>
<?php

if(isset($_POST['submit'])){
    $filesdoc = $_FILES['file']['tmp_name'];
    $filesdocname = 'greenoffice'.round(microtime(true)*9999);
    $path = "../../../assets/files/allfiles/";
    $arraytemp = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

    if($arraytemp=="pdf" or $arraytemp=="doc" or $arraytemp=="docx"){
        if(move_uploaded_file($filesdoc,$path.$filesdocname.'_edit.'. $arraytemp)){ 
            $sql = "UPDATE `tb_gf` 
            SET title_gf = '".$_POST['title_gf']."', 
                link_gf = '".$_POST['link_gf']."', 
                file_gf = '".$filesdocname.'_edit.'.$arraytemp."', 
                type_gf = '".$_POST['type_gf']."', 
                update_at = '".date('Y-m-d H:i:s')."'
                WHERE id_gf = '".$_POST['id']."' ";
            $result = $conn->query($sql) or die($conn->error);
            if($result){
                echo '<script> alert("แก้ไขข้อมูลสำเร็จ") </script>';
                header('Refresh:0; url=index.php');
            }else{
                echo '<script> alert("Error Creating!")</script>'; 
                header('Refresh:0; url=index.php');
            }
        }
    } else {
        echo '<script> alert("ไฟล์ที่ท่านอัพโหลดไม่ใช่นามสกุลไล์ .pdf .docx .doc !")</script>'; 
        header('Refresh:0; url=index.php');
        }
} else {
    header('Refresh:0; url=index.php');
}
?>