<?php 

$file_name = basename($_SERVER['SCRIPT_FILENAME'],".php");

?>
<!-- ส่วนที่ 2 -->
<!-- <canvas class="snow" id="snow" width="1848" height="515"></canvas> -->
<!-- ส่วนที่ 2 -->
<section>
    <footer class="semi-footer p-5 text-center text-md-left ">
        <div class="row ">
            <div class="col-md-4 ">
                <a class="navbar-brand " href="index.php">
                    <img src="assets/image/logo.png " width="35 " height="35 " class="d-inline-block align-top "
                        alt="ScitechVRU "> Scitech VRU
                </a>
                <p><i class="fa fa-address-card"></i> คณะวิทยาศาสตร์และเทคโนโลยี
                    <br>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์<br> เลขที่ 1 หมู่ 20 ถนนพหลโยธิน กม. 48
                    ต.คลองหนึ่ง อ.คลองหลวง จ.ปทุมธานี 13180
                    <br><i class="fa fa-phone-square"></i> 0-2529-3850 , ภายในเบอร์ : 161 <br><i
                        class="fa fa-phone-square"></i> โทรสาร : 02-909-3029 <br> <i class="fa fa-phone-square"></i>
                    09-2265-8433<br>

                    <i class="fa fa-envelope"></i> sciencetech@vru.ac.th<br>
                </p>
                <a href="https://www.facebook.com/SciTechvru2018 "><i class="fab fa-facebook-square fa-2x "></i></a>
                <a href="https://www.youtube.com/channel/UCQW9awn5wFgFPotUk3XWWKg " target="_blank "><i
                        class="fab fa-youtube-square fa-2x "></i></a>
            </div>
            <div class="col-md-4 ">
                <h4>เมนู</h4>
                <ul class="navbar-nav ">
                    <li class="nav-item <?php echo $file_name == 'index' ? 'active': '' ?>">
                        <a class="nav-link" href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="nav-item <?php echo $file_name == 'about' ? 'active': '' ?>">
                        <a class="nav-link" href="about.php">เกี่ยวกับเรา</a>
                    </li>
                    <li
                        class="nav-item <?php echo $file_name == 'blog' || $file_name == 'blog-detail' ? 'active': '' ?>">
                        <a class="nav-link" href="blog.php">ข่าวสารและกิจกรรม</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://forms.gle/4WYGkwpKAhX387Du6" target="_blank"
                            rel="noopener noreferrer">สมัครเรียน</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://sci.vru.ac.th/home.php" target="_blank"
                            rel="noopener noreferrer">เว็บไซต์เดิม</a>
                    </li>
                    <li class="nav-item <?php echo $file_name == 'contact' ? 'active': '' ?>">
                        <a class="nav-link" href="contact.php">ติดต่อ</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 ">
                <h4>แผนที่</h4>
                <div class="mapouter ">
                    <div class="gmap_canvas ">
                        <iframe width="100% " height="300 " id="gmap_canvas "
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d967.2608178627878!2d100.61434287306969!3d14.133552924779382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e27f5733db0bbd%3A0x729599c7dd54f2da!2z4Lir4LmJ4Lit4LiH4Lib4Lij4Liw4LiK4Li44Lih4Lij4Liy4LiK4Lie4Lik4LiB4Lip4LmMIOC4hOC4k-C4sOC4p-C4tOC4l-C4ouC4suC4qOC4suC4quC4leC4o-C5jOC5geC4peC4sOC5gOC4l-C4hOC5guC4meC5guC4peC4ouC4tQ!5e0!3m2!1sth!2sth!4v1639362264317!5m2!1sth!2sth"
                            frameborder="0 " scrolling="no
                            " marginheight="0 " marginwidth="0 "></iframe>
                    </div>
                    <style>
                    .mapouter {
                        text-align: right;
                        height: 300px;
                        width: 100%;
                    }

                    .gmap_canvas {
                        overflow: hidden;
                        background: none !important;
                        height: 300px;
                        width: 100%;
                    }
                    </style>
                </div>
            </div>
        </div>
    </footer>
    <footer class="footer ">
        <span>
            Copyright © 2022 <a href="https://sci.ac.th/ScitechVRU/ " target="_blank ">Scitech VRU</a>
            All Right Reserved
        </span>
    </footer>
</section>
<!-- snow -->
<script>
/* 
(function() {

    var canvas, ctx;
    var points = [];
    var maxDist = 100;

    function init() {
        //Add on load scripts
        canvas = document.getElementById("snow");
        ctx = canvas.getContext("2d");
        resizeCanvas();
        pointFun();
        setInterval(pointFun, 20);
        window.addEventListener('resize', resizeCanvas, false);
    }
    //Particle constructor
    function point() {
        this.x = Math.random() * (canvas.width + maxDist) - (maxDist / 2);
        this.y = Math.random() * (canvas.height + maxDist) - (maxDist / 2);
        this.z = (Math.random() * 0.5) + 0.5;
        this.vx = ((Math.random() * 2) - 0.5) * this.z;
        this.vy = ((Math.random() * 1.5) + 1.5) * this.z;
        this.fill = "rgba(255,255,255," + ((0.4 * Math.random()) + 0.5) + ")";
        this.dia = ((Math.random() * 2.5) + 1.5) * this.z;
        points.push(this);
    }
    //Point generator
    function generatePoints(amount) {
        var temp;
        for (var i = 0; i < amount; i++) {
            temp = new point();
        };
        // console.log(points);
    }
    //Point drawer
    function draw(obj) {
        ctx.beginPath();
        ctx.strokeStyle = "transparent";
        ctx.fillStyle = obj.fill;
        ctx.arc(obj.x, obj.y, obj.dia, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.stroke();
        ctx.fill();
    }
    //Updates point position values
    function update(obj) {
        obj.x += obj.vx;
        obj.y += obj.vy;
        if (obj.x > canvas.width + (maxDist / 2)) {
            obj.x = -(maxDist / 2);
        } else if (obj.xpos < -(maxDist / 2)) {
            obj.x = canvas.width + (maxDist / 2);
        }
        if (obj.y > canvas.height + (maxDist / 2)) {
            obj.y = -(maxDist / 2);
        } else if (obj.y < -(maxDist / 2)) {
            obj.y = canvas.height + (maxDist / 2);
        }
    }
    //
    function pointFun() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for (var i = 0; i < points.length; i++) {
            draw(points[i]);
            update(points[i]);
        };
    }

    function resizeCanvas() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        points = [];
        generatePoints(window.innerWidth / 3);
        pointFun();
    }

    //Execute when DOM has loaded
    document.addEventListener('DOMContentLoaded', init, false);
})();*/
</script> 
<!-- end snow -->

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap"></script>