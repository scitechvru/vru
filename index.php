<?php

    require_once('php/connect.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- COMMON TAGS -->
    <meta charset="UTF-8">
    <title>Scitech VRU</title>
    <meta name="google-site-verification" content="9PR-_y1nMZeXzVEDJgarF2uOc0qzhM1JBMVILFCl5nA" />
    <!-- ทำให้บนมือถือซูมไม่ได้ (เฉพาะบางเบราเซอร์)-->
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!-- CSS img gallery Ekko Lightbox-->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="node_modules/ekko-lightbox/dist/ekko-lightbox.css">
    <link rel="stylesheet" href="node_modules/node-snackbar/dist/snackbar.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/image/favicon/site.webmanifest">
    <link rel="mask-icon" href="assets/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="assets/image/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="assets/image/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Search Engine -->
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="keywords"
        content="คณะวิทย์ฯ, scitech, sci,vru,techno, information, computer, physics, chemistry, bio,ราชการ, สมัครงาน, joomla, จัดซื้อจัดจ้าง, บันทึกข้อความ, เรียนต่อ, โควตา, รับตรง, ภาคี,เทคโน, เทคคอม, มัลติ, มัลติมีเดีย, มีเดีย, การจัดการคอม, ห้องเรียนคอม, เฟรชชี่, น้องใหม่, นักศึกษา, วิทยาศาสตร์, สำนักงานคณะวิทย์, วิทย์, วิทยฯ,มหาวิทยาลัยราชภัฏ, ศูนย์วิทย์, ฟิสิกส์, ฟิสิก, ชีววะ, เคมี, สิ่งแวดล้อม, เทคโนโลยีสารสนเทศ, มหาวิทยาลัย,ราชภัฏ,วไลยอลงกรณ์,ในพระบรมราชูปถัมภ์,ปริญญาตรี,ปริญญาโท,เรียนต่อปริญญาโท,เรียนต่อปริญญาเอก">
    <meta name="robots" content="index, nofollow">
    <meta name="language" content="English">
    <meta name="author" content="ScitechVRU">
    <!-- Schema.org for Google -->
    <meta name="name" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="fb:app_id" content="620851126015605">
    <meta name="og:title" content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์">
    <meta name="og:description"
        content="คณะวิทยาศาสตร์และเทคโนโลยี มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมภ์ เปิดหลักสูตรปริญญาตรี โท และเอก เปิดรับสมัครนักศึกษาปริญญาตรี โท และเอก">
    <meta name="og:image" content="https://www.facebook.com/SciTechvru2018/photos/a.1739709372734354/3051967498175195/">
    <meta name="og:url" content="http://sci.vru.ac.th/">
    <meta name="og:type" content="website">

</head>

<body>
    <!-- Section Navbar -->
    <?php include_once('includes/navbar.php');?>
    <!-- End Section Navbar -->

    <!-- Section Carousel -->
    <section id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="carousel-img" style="background-image: url(assets/image/img/ScitechVRU5.jpg) ;">
                    <div class="backscreen"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="carousel-img" style="background-image: url(assets/image/img/8.jpg) ;">

                    <div class="backscreen"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="carousel-img" style="background-image: url(assets/image/img/3.jpg) ;">
                    <div class="backscreen"></div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </section>

    <!-- Section Hope -->
    <section class="jumbotron jumbotron-fluid text-center">
        <div class="container">
            <h1 class="border-short-bottom">สมรรถนะหลัก</h1>
            <p class="lead">บูรณาการพันธกิจสัมพันธ์เพื่อพัฒนาท้องถิ่นด้วยนวัตกรรมทางวิทยาศาสตร์และเทคโนโลยี</p>
        </div>
    </section>

    <!-- Section ShortCoures -->
    <section class="container">
        <h1 class="border-short-bottom text-center">หลักสูตรระยะสั้น</h1>

        <div class="row ">
            <?php

            $no = 1;
            $sql = "SELECT * FROM tb_scoures WHERE status_scoures = 'true' order by `start_date` desc";
            $result = $conn->query($sql);
            if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
            ?>
            <section class="col-12 col-sm-6 col-md-4 p-2">
                <div class="card h-100">
                    <a href="<?php echo $row['link_scoures'];?>" target="_blank" rel="noopener noreferrer"
                        class="warpper-card-img">
                        <img class="card-img-top" src="assets/images/shortcoures/<?php echo $row['img_scoures'];?>"
                            alt="GeniusScienceLab">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['name_scoures'];?></h5>
                        <p class="card-text"><?php echo $row['detail_scoures'];?></p>
                        <p class="card-text">
                            <?php echo date_format(new DateTime($row['status_scoures']),"j F Y").'TO'.date_format(new DateTime($row['status_scoures']),"j F Y");?>
                        </p>
                    </div>
                    <div class="p-3">
                        <a href="<?php echo $row['link_scoures'];?>" target="_blank" rel="noopener noreferrer"
                            class="btn btn-primary btn-block">ลงทะเบียน</a>
                    </div>
                </div>
            </section>
            <?php
            if ($no == 5) {
                break;
            } //endif
                $no++;
            
                } //endwhile
            } //end if
            ?>
        </div>

    </section>

    <!-- Section Activites -->
    <section class="container">
        <h1 class="border-short-bottom text-center">กิจกรรม</h1>
        <?php 

        $no = 0;
        $title_img1 = "";
        $sql = "SELECT * FROM tb_picup WHERE major_img='OF' order by date_img desc";
        $result = $conn->query($sql);
      

    ?>
        <div class="row">
            <?php 
              if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
                    if ($title_img1 != $row['title_img']) {
                        $title_img1 = $row['title_img'];
                        
        ?>
            <section class="col-12 col-sm-6 col-md-4 p-2">
                <div class="card h-100">
                    <a href="blog-detail.php?id=<?php echo $row['cover_img']?>" class="warpper-card-img">
                        <img class="card-img-top" src="assets/images/activity/<?php echo $row['cover_img']?>"
                            alt="<?php echo $row['title_img'] ?>">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $title_img1; ?></h5>
                        <p class="card-text"><i class="fa fa-calendar"></i>
                            <?php echo date_format(new DateTime($row['date_img']),"j F Y");?></p>
                        <p class="card-text"><i class="fa fa-flag"></i> <?php echo $row['place_img']?></p>
                    </div>
                    <div class="p-3">
                        <a href="blog-detail.php?id=<?php echo $row['cover_img']?>"
                            class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <?php 
                        if ($no == 5) {
                            break;
                        }
                        $no++;
                    }
                } //end loop
              }
            ?>
        </div>
    </section>

    <!-- Section article -->
    <section class="container py-5">
        <h1 class="border-short-bottom text-center">การวิจัยและวรสาร</h1>

        <div class="owl-carousel owl-theme">
            <?php

            $no = 1;
            $sql = "SELECT * FROM articles WHERE `status` = 'true' order by `created_at` desc";
            $result = $conn->query($sql);
            if ($result->num_rows > 0){
                while ($row = $result->fetch_assoc()){
            ?>
            <section class="col-12 p-2">
                <div class="card h-100">
                    <a href="<?php echo $row['link'];?>" target="_blank" rel="noopener noreferrer"
                        class="warpper-card-img">
                        <img class="card-img-top" src="assets/images/articles/<?php echo $row['image'];?>" alt="">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['subject'];?></h5>
                        <p class="card-text"><?php echo $row['detail'];?></p>
                    </div>
                    <div class="p-3">
                        <a href="<?php echo $row['link'];?>" target="_blank" rel="noopener noreferrer"
                            class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <?php
            if ($no == 5) {
                break;
            } //endif
                $no++;
            
                } //endwhile
            } //end if
            ?>

        </div>

    </section>

    <!-- Section Release video -->
    <section class="container py-5">
        <h1 class="border-short-bottom text-center">สื่อวิดีโอเผยแพร่</h1>
        <div class="owl-carousel owl-theme">
            <?php

        $no = 1;
        $code_youtube = "";
        $sql = "SELECT * FROM tb_youtube WHERE major_youtube='OF' order by date_youtube desc";
        $result = $conn->query($sql);
        if ($result->num_rows > 0){
            while ($row = $result->fetch_assoc()){
                if ($code_youtube != $row['youtube_code']) {
                    $code_youtube = $row['youtube_code'];
        ?>
            <section class="col-12 p-2">
                <div class="card h-100 ">
                    <div class="embed-responsive embed-responsive-16by9 ">
                        <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/<?php echo $code_youtube;?>"></iframe>
                    </div>
                </div>
            </section>
            <?php
            if ($no == 5) {
                break;
            } //endif
            $no++;
            } //end if
                } //endwhile
            } //end if

            ?>
        </div>
    </section>

    <!-- Section Normal -->
    <section class="container py-5">
        <h1 class="border-short-bottom text-center">หมวดทั่วไป</h1>
        <div class="row ">
            <section class="col-12 col-sm-6 col-md-6 p-2 ">
            <div class="card h-100">
                    <a href="people.php" target="_blank" rel="noopener noreferrer" class="warpper-card-img">
                        <img class="card-img-top" src="assets/image/img/ScitechVRU5.jpg" alt="">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title">ทำเนียบบุคลากร</h5>
                        <p class="card-text"></p>
                    </div>
                    <div class="p-3">
                        <a href="people.php" target="_blank" rel="noopener noreferrer"
                            class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <section class="col-12 col-sm-6 col-md-6 p-2 ">
                <div class="card h-100 ">
                    <a href="#" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/ScitechVRU5.jpg" alt="">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">บอร์ดผู้บริหาร</h5>
                        <p class="card-text "></p>
                    </div>
                    <div class="p-3 ">
                        <a href="#" class="btn btn-primary btn-block ">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <!-- Section UnderScitechVRU -->
    <section class="container py-5">
        <h1 class="border-short-bottom text-center">หน่วยงานภายใน</h1>

        <div class="owl-carousel owl-theme">
            <?php

        $sql = "SELECT * FROM tb_major WHERE int_major != 'OF' AND status_major = 'true' ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0){
            while ($row = $result->fetch_assoc()){
                
        ?>
            <section class="col-12 p-2">
                <div class="card h-100">
                    <a href="<?php echo $row['link_major'];?>" target="_blank" rel="noopener noreferrer"
                        class="warpper-card-img">
                        <img class="card-img-top" src="assets/image/img/ScitechVRU5.jpg" alt="">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['nameth_major'];?></h5>
                        <p class="card-text"><?php echo $row['nameen_major'];?></p>
                    </div>
                    <div class="p-3">
                        <a href="<?php echo $row['link_major'];?>" target="_blank" rel="noopener noreferrer"
                            class="btn btn-primary btn-block">เข้าสู่เว็บไซต์</a>
                    </div>
                </div>
            </section>
            <?php

            $no++;
            
                } //endwhile
            } //end if

            ?>

        </div>

    </section>
    <!-- Section MENU FAC -->
    <section class="container py-5 ">
        <h1 class="border-short-bottom text-center ">เมนูสำหรับอาจารย์และบุคลากร</h1>
        <div class="row ">
            <section class="col-12 col-sm-6 col-md-6 p-2 ">
                <div class="card h-100 ">
                    <a href="#" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru11.jpg" alt="">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">อาจารย์และบุคลากร</h5>
                        <p class="card-text "></p>
                    </div>
                    <div class="p-3 ">
                        <a href="#" class="btn btn-primary btn-block ">เข้าสู่ระบบ</a>
                    </div>
                </div>
            </section>
            <section class="col-12 col-sm-6 col-md-6 p-2 ">
                <div class="card h-100 ">
                    <a href="admin/login.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru6.jpg" alt="">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">ผู้ดูแลระบบ</h5>
                        <p class="card-text "></p>
                    </div>
                    <div class="p-3 ">
                        <a href="admin/login.php" class="btn btn-primary btn-block ">เข้าสู่ระบบ</a>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <!-- Section Services and information -->
    <section class="container py-5 ">
        <h1 class="border-short-bottom text-center ">บริการและข้อมูลอื่นๆ</h1>
        <div class="row ">
            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="ita.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru15.jpg"
                            alt="การประเมินคุณธรรมและความโปร่งใส">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">ITA</h5>
                        <p class="card-text ">การประเมินคุณธรรมและความโปร่งใส</p>
                    </div>
                    <div class="p-3 ">
                        <a href="ita.php" class="btn btn-primary btn-block ">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="#" class="warpper-card-img " data-toggle="modal" data-target="#files">
                        <img class="card-img-top " src="assets/image/img/appvru7.jpg" alt="แบบฟอร์ม / คู่มือ">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">แบบฟอร์ม / คู่มือ</h5>
                        <p class="card-text ">ดาวน์โหลดแบบฟอร์ม / คู่มือ สำหรับ อาจารย์ บุคลากรและนักศึกษา</p>
                    </div>
                    <div class="p-3 ">
                        <a href="#" class="btn btn-primary btn-block " data-toggle="modal"
                            data-target="#files">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="# " class="warpper-card-img " data-toggle="modal" data-target="#report">
                        <img class="card-img-top " src="assets/image/img/appvru9.jpg" alt="วาระการประชุม">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">วาระการประชุม</h5>
                        <p class="card-text ">วาระการประชุมทั้งหมด</p>
                    </div>
                    <div class="p-3 ">
                        <a href="# " class="btn btn-primary btn-block " data-toggle="modal"
                            data-target="#report">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="# " class="warpper-card-img " data-toggle="modal" data-target="#online">
                        <img class="card-img-top " src="assets/image/img/appvru4.jpg" alt="บริการออนไลน์">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">บริการออนไลน์</h5>
                        <p class="card-text ">บริการออนไลน์ที่ให้บริการ</p>
                    </div>
                    <div class="p-3 ">
                        <a href="# " class="btn btn-primary btn-block " data-toggle="modal"
                            data-target="#online">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="https://bit.ly/33VSqhl" target="_blank" rel="noopener noreferrer"
                        class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru10.jpg" alt="วารสารJRIST">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">วารสารJRIST</h5>
                        <p class="card-text ">ระบบวารสารJRIST</p>
                    </div>
                    <div class="p-3 ">
                        <a href="https://bit.ly/33VSqhl" target="_blank" rel="noopener noreferrer"
                            class="btn btn-primary btn-block ">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="safety.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru8.jpg" alt="สถานศึกษาปลอดภัย">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">สถานศึกษาปลอดภัย</h5>

                    </div>
                    <div class="p-3 ">
                        <a href="safety.php" class="btn btn-primary btn-block ">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="greenoffice.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru3.jpg" alt="GREEN OFFICE">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">GREEN OFFICE</h5>

                    </div>
                    <div class="p-3 ">
                        <a href="greenoffice.php" class="btn btn-primary btn-block ">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="procurement.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru1.jpg" alt="จัดซื้อจัดจ้าง">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">จัดซื้อจัดจ้าง</h5>

                    </div>
                    <div class="p-3 ">
                        <a href="procurement.php" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>
            <section class="col-12 col-sm-6 col-md-4 p-2 ">
                <div class="card h-100 ">
                    <a href="plan.php" class="warpper-card-img ">
                        <img class="card-img-top " src="assets/image/img/appvru5.jpg" alt="แผนยุทธศาสตร์">
                    </a>
                    <div class="card-body ">
                        <h5 class="card-title ">แผนยุทธศาสตร์</h5>

                    </div>
                    <div class="p-3 ">
                        <a href="plan.php" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                    </div>
                </div>
            </section>

        </div>

    </section>

    <section class="container py-5 ">
        <h1 class="border-short-bottom text-center ">กำหนดการลงทะเบียนเรียน</h1>
        <div class="row">

            <div class="col-md-4">
                <a href="assets/image/calendar/calendar1.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคการศึกษาที่ 1/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar1.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคการศึกษาที่ 1/2565">
                </a>
            </div>
            <div class="col-md-4">
                <a href="assets/image/calendar/calendar2.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคการศึกษาที่ 2/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar2.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคการศึกษาที่ 2/2565">
                </a>
            </div>
            <div class="col-md-4">
                <a href="assets/image/calendar/calendar3.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคฤดูร้อน/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar3.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา จันทร์-ศุกร์ ภาคฤดูร้อน/2565">
                </a>
            </div>
            <div class="col-md-4">
                <a href="assets/image/calendar/calendar4.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคการศึกษาที่ 1/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar4.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคการศึกษาที่ 1/2565">
                </a>
            </div>
            <div class="col-md-4">
                <a href="assets/image/calendar/calendar5.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคการศึกษาที่ 2/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar5.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคการศึกษาที่ 2/2565">
                </a>
            </div>
            <div class="col-md-4">
                <a href="assets/image/calendar/calendar6.jpg" data-toggle="lightbox"
                    data-title="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคฤดูร้อน/2565"
                    data-gallery="gallery">
                    <img class="img-fluid mb-2" src="assets/image/calendar/calendar6.jpg"
                        alt="ปฏิทินการศึกษาของนักศึกษาระดับปริญญาตรีเต็มเวลา เสาร์-อาทิตย์ ภาคฤดูร้อน/2565">
                </a>
            </div>

        </div>

    </section>


    <!-- แบบฟอร์ม / คู่มือ -->
    <div class="modal fade" id="files" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แบบฟอร์ม / คู่มือ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="form_personal.php?id=stu" class="list-group-item list-group-item-action "
                            aria-current="true">
                            นักศึกษา
                        </a>
                        <a href="form_personal.php?id=man"
                            class="list-group-item list-group-item-action ">คู่มือนักศึกษา</a>
                        <a href="form_personal.php?id=per"
                            class="list-group-item list-group-item-action ">อาจารย์/เจ้าหน้าที่</a>
                        <a href="form_personal.php?id=reg"
                            class="list-group-item list-group-item-action ">ระเบียบ/ข้อบังคับ/กฎหมาย/ประกาศ</a>
                        <a href="form_personal.php?id=res" class="list-group-item list-group-item-action ">วิจัย</a>
                        <a href="form_personal.php?id=info" class="list-group-item list-group-item-action ">สารสนเทศ</a>
                        <a href="form_personal.php?id=mua" class="list-group-item list-group-item-action ">คู่มือ</a>
                        <a href="form_personal.php?id=jr1" class="list-group-item list-group-item-action">วารสารJRIST
                            ระเบียบการตีพิมพ์</a>
                        <a href="form_personal.php?id=jr2" class="list-group-item list-group-item-action ">วารสารJRIST
                            รูปแบบบทความวิชาการ</a>
                        <a href="form_personal.php?id=jr3" class="list-group-item list-group-item-action ">วารสารJRIST
                            รูปแบบบทความวิจัย</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>

            </div>
        </div>
    </div>

    <!-- วาระการประชุม -->
    <div class="modal fade" id="report" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ระเบียบวาระการประชุม</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="report.php?id=1" class="list-group-item list-group-item-action " aria-current="true">
                            ระเบียบวาระการประชุมคณะกรรมการวิชาการคณะ
                        </a>
                        <a href="report.php?id=2"
                            class="list-group-item list-group-item-action">ระเบียบวาระการประชุมคณะกรรมการบริหารคณะ</a>
                        <a href="report.php?id=3" class="list-group-item list-group-item-action">คณบดีสัญจร</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>

            </div>
        </div>
    </div>

    <!-- บริการออนไลน์-->
    <div class="modal fade" id="online" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">บริการออนไลน์</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="https://forms.gle/kWA2SFtkCJCLyQsEA" target="_blank" rel="noopener noreferrer"
                            class="list-group-item list-group-item-action " aria-current="true">
                            จองห้องประชุมออนไลน์
                        </a>
                        <a href="https://forms.gle/2T2SYRwYWNaZsGYC9" target="_blank" rel="noopener noreferrer"
                            class="list-group-item list-group-item-action">จองห้องปฏิบัติการทักษะกระบวนการทางวิทยาศาสตร์
                            (ห้อง 5103)</a>
                        <a href="http://acad.vru.ac.th/about_acad/ac_calendar_Schedule_register.php" target="_blank"
                            rel="noopener noreferrer" class="list-group-item list-group-item-action">ปฏิทินวิชาการ /
                            กำหนดการลงทะเบียนเรียน</a>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="intro" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ประชาสัมพันธ์</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php 
                    //คิวรี่ข้อมูลมาแสดงในตาราง
                    $sql = "SELECT * FROM tb_publicize WHERE status = 'true'";
                    $result = $conn->query($sql);
                    $row = $result->fetch_assoc();
                    ?>
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php
                            //กำหนด class active เพื่อเรียกใช้ button สำหรับคลิกสไลด์
                              $i=0;
                              foreach($result as $row){
                              $actives='';
                              if($i==0){
                              $actives='active';
                              }
                              ?>
                            <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $i;?>"
                                class="<?php echo $actives;?>"></li>
                            <?php $i++; } ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php
                            //กำหนด class active สำหรับเรียกรูปมาแสดง
                              $i=0;
                              foreach($result as $row){
                              $actives='';
                              if($i==0){
                              $actives='active';
                              }
                              ?>
                            <div class="carousel-item <?php echo $actives;?>">
                                <a href="<?php echo $row['link_public'];?>" target="_blank" rel="noopener noreferrer">
                                    <img src="assets/images/publicbanner/<?php echo $row['file_public'];?>"
                                        class="d-block w-100" alt="<?php echo $row['title_public'];?>">
                                </a>
                            </div>
                            <?php $i++; } ?>

                        </div>
                        <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                </div>

            </div>
        </div>
    </div>

    <!-- Section footer -->
    <?php include_once('includes/footer.php');?>
    <?php include_once('php/userlogs.php') ?>
    <!-- End Section footer -->

    <!-- Messenger ปลั๊กอินแชท Code -->
    <div id="fb-root"></div>

    <!-- Your ปลั๊กอินแชท code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>


    <!-- Section On to Top -->
    <div class="to-top ">
        <i class="fa fa-angle-up " aria-hidden="true "></i>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js "></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js "></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js "></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js "></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.min.js "></script>
    <!-- Sncak bar -->
    <script src="node_modules/node-snackbar/dist/snackbar.min.js"></script>
    <!-- CSS img gallery Ekko Lightbox -->
    <script src="node_modules/ekko-lightbox/dist/ekko-lightbox.min.js"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7pVYXyLuKkftPkDMFhpTjov4MYVxTnY&callback=initMap ">
    </script>
    <script src="assets/js/main.js "></script>

    <script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "1739708992734392");
    chatbox.setAttribute("attribution", "biz_inbox");
    </script>
    <!-- Your SDK code -->
    <script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v12.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    dots: false
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    });

    //              DO NOT IMPLEMENT                //
    //       this code through the following        //
    //                                              //
    //   Floodlight Pixel Manager                   //
    //   DCM Pixel Manager                          //
    //   Any system that places code in an iframe   //
    (function() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i') +
            '.po.st/static/v4/post-widget.js#publisherKey=556iuqt4139l475oo1e8';
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
    /*  Page specific script  */
    $(function() {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

        $('.filter-container').filterizr({
            gutterPixels: 3
        });
        $('.btn[data-filter]').on('click', function() {
            $('.btn[data-filter]').removeClass('active');
            $(this).addClass('active');
        });
    })

    $(document).ready(function() {
        setTimeout(function() {
            $("#intro").modal('show');
        }, 1000);
    });

    $(document).ready(function() {

        Snackbar.show({
            text: 'Your privacy is important to us. We need your data just for the important process of services. Please allow if you accept the term of privacy to comply with PDPA. by ScitechVRU Read term and privacy policy',
            width: 'auto',
            actionText: 'Accept All',
            actionTextColor: '#75c6fc',
            backgroundColor: '#fff',
            textColor: '#000',
            duration: 500000000,

        });

    });
    </script>
</body>

</html>