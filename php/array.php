<?php 
$thaimonth=array(
    "00"=>"",
"01"=>"มกราคม",
"02"=>"กุมภาพันธ์",
"03"=>"มีนาคม",
"04"=>"เมษายน",
"05"=>"พฤษภาคม",
"06"=>"มิถุนายน",
"07"=>"กรกฎาคม",
"08"=>"สิงหาคม",
"09"=>"กันยายน",
"10"=>"ตุลาคม",
"11"=>"พฤศจิกายน",
"12"=>"ธันวาคม");
$thaimonth1=array(
    "00"=>"",
    "01"=>"ม.ค.",
    "02"=>"ก.พ.",
    "03"=>"มี.ค.",
    "04"=>"เม.ย.",
    "05"=>"พ.ค.",
    "06"=>"มิ.ย.",
    "07"=>"ก.ค.",
    "08"=>"ส.ค.",
    "09"=>"ก.ย.",
    "10"=>"ต.ค.",
    "11"=>"พ.ย.",
    "12"=>"ธ.ค."
);
$level_edu=array(
    "1"=>"ปริญญาตรี",
    "2"=>"ปริญญาโท",
    "3"=>"ปริญญาเอก"
);
$type_perdo=array(
    "1"=>"เอกสารประกอบการสอน",
    "2"=>"เอกสารคำสอน",
    "3"=>"ตำรา",
    "4"=>"หนังสือ"
);
$type_perjo=array(
    "1"=>"วิชาการ",
    "2"=>"วิจัย"
);
$level_perjo=array(
    "na"=>"National Journal",
    "in"=>"International Journal"
);
$level_perpros=array(
    "na"=>"การประชุมวิชาการระดับชาติ",
    "in"=>"การประชุมวิชาการระดับนานาชาติ"
);
$money_perre=array(
    "na"=>"ภายใน",
    "in"=>"ภายนอก"
);
$satre_perre=array(
    "na"=>"หัวหน้าโครงการ",
    "in"=>"ผู้ร่วมวิจัย"
);
$satwo_perre=array(
    "p"=>"กำลังดำเนินการ",
    "f"=>"สำเร็จ"
);
$media_peron=array(
    "1"=>"วิทยุ",
    "2"=>"โทรทัศน์",
    "3"=>"Website",
    "4"=>"แหล่งอื่นๆ"
);
$type_perint=array(
    "1"=>"สิทธิบัตร",
    "2"=>"อนุสิทธิบัตร",
    "3"=>"ลิขสิทธิ์ผลงาน",
    "4"=>"เครื่องหมายการค้า"
);

?>